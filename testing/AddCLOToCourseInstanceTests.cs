using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;
using repository;
using domain.CourseLearningOutcomeAggregate;
using domain.CourseInstanceAggregate;
using domain.AcademicYearAggregate;
using domain.CourseAggregate;
using domain.SemesterAggregate;
using System.Collections.Generic;


namespace testing
{
    public class AddCLOToCourseInstanceTests
    {

        [Fact]
        public void AddOneCLOToCourseInstance()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "AddOneCLOToCourseInstance_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // Create a CourseInstance
                var courseInstance = new CourseInstance()
                {
                    CourseInstanceId = "439001",
                    AcademicYearId = "2020-2021",
                    SemesterId = "Spring",
                    CourseId = "CIDM4390",
                    CourseLearningOutcomes = new List<CourseLearningOutcome> {
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO1", 
                            CourseLearningOutcomeDesc = "Analyze an organizational information system need to elicit system requirements.",
                            CourseInstanceId = "439001",
                            CacStudentOutcomeId = "SO1"
                        }
                    }
                };
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances.Select(x => x.CourseLearningOutcomes.Take(1));
                // Assert that the CourseLearningOutcome property is not null inside CourseInstance (e.g. it includes CLO1)
                Assert.NotNull(result);
            }
        }

        [Fact]
        public void AddTwoCLOToCourseInstance()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "AddTwoCLOToCourseInstance_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // Create a CourseInstance
                var courseInstance = new CourseInstance()
                {
                    CourseInstanceId = "437270",
                    AcademicYearId = "2020-2021",
                    SemesterId = "Spring",
                    CourseId = "CIDM4372",
                    CourseLearningOutcomes = new List<CourseLearningOutcome> {
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO1", 
                            CourseLearningOutcomeDesc = "Apply a visualization design process to create effective visualizations.",
                            CourseInstanceId = "437270",
                            CacStudentOutcomeId = "SO2"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO2", 
                            CourseLearningOutcomeDesc = "Critically evaluate visualizations and suggest improvements and refinements.",
                            CourseInstanceId = "437270",
                            CacStudentOutcomeId = "SO2"
                        }
                    }
                };
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances.Select(x => x.CourseLearningOutcomes.Take(2));
                // Assert that the CourseLearningOutcome property is not null inside CourseInstance (e.g. it includes CLO1)
                Assert.NotNull(result);
            }
        }

        [Fact]
        public void AddThreeCLOToCourseInstance()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "AddThreeCLOToCourseInstance_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // Create a CourseInstance
                var courseInstance = new CourseInstance()
                {
                    CourseInstanceId = "4231570",
                    AcademicYearId = "2021-2022",
                    SemesterId = "Fall",
                    CourseId = "CIDM2315",
                    CourseLearningOutcomes = new List<CourseLearningOutcome> {
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO1", 
                            CourseLearningOutcomeDesc = " Implement operators and control structures for logical design.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO2"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO2", 
                            CourseLearningOutcomeDesc = "Modularize programming code with methods.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO2"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO3", 
                            CourseLearningOutcomeDesc = "Implement data structures.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO2"
                        }
                    }
                };
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances.Select(x => x.CourseLearningOutcomes.Take(3));
                // Assert that the CourseLearningOutcome property is not null inside CourseInstance (e.g. it includes CLO1)
                Assert.NotNull(result);
            }
        }

        [Fact]
        public void AddFourCLOToCourseInstance()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "AddFourCLOToCourseInstance_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // Create a CourseInstance
                var courseInstance = new CourseInstance()
                {
                    CourseInstanceId = "4231570",
                    AcademicYearId = "2020-2021",
                    SemesterId = "Spring",
                    CourseId = "CIDM3320",
                    CourseLearningOutcomes = new List<CourseLearningOutcome> {
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO1", 
                            CourseLearningOutcomeDesc = "Analyze an organizational information system need to elicit system requirements.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO4"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO2", 
                            CourseLearningOutcomeDesc = "Execute an appropriate SDLC process model.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO4"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO3", 
                            CourseLearningOutcomeDesc = "Given system requirements, design and implement an appropriate system architecture",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO4"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO4", 
                            CourseLearningOutcomeDesc = "Use source code management tools.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO4"
                        }
                    }
                };
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances.Select(x => x.CourseLearningOutcomes.Take(4));
                // Assert that the CourseLearningOutcome property is not null inside CourseInstance (e.g. it includes CLO1)
                Assert.NotNull(result);
            }
        }

        [Fact]
        public void AddFiveCLOToCourseInstance()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "AddFiveCLOToCourseInstance_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // Create a CourseInstance
                var courseInstance = new CourseInstance()
                {
                    CourseInstanceId = "4231570",
                    AcademicYearId = "2020-2021",
                    SemesterId = "Spring",
                    CourseId = "CIDM3320",
                    CourseLearningOutcomes = new List<CourseLearningOutcome> {
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO1", 
                            CourseLearningOutcomeDesc = "Analyze an organizational information system need to elicit system requirements.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO5"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO2", 
                            CourseLearningOutcomeDesc = "Execute an appropriate SDLC process model.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO5"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO3", 
                            CourseLearningOutcomeDesc = "Given system requirements, design and implement an appropriate system architecture.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO5"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO4", 
                            CourseLearningOutcomeDesc = "Use source code management tools.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO5"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO5", 
                            CourseLearningOutcomeDesc = "Negotiate goals and conditions of satisfaction for a software project with clients and stakeholders.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO5"
                        }
                    }
                };
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances.Select(x => x.CourseLearningOutcomes.Take(5));
                // Assert that the CourseLearningOutcome property is not null inside CourseInstance (e.g. it includes CLO1)
                Assert.NotNull(result);
            }
        }

        [Fact]
        public void AddSixCLOToCourseInstance()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "AddSixCLOToCourseInstance_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // Create a CourseInstance
                var courseInstance = new CourseInstance()
                {
                    CourseInstanceId = "4231570",
                    AcademicYearId = "2020-2021",
                    SemesterId = "Spring",
                    CourseId = "CIDM3320",
                    CourseLearningOutcomes = new List<CourseLearningOutcome> {
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO1", 
                            CourseLearningOutcomeDesc = "Analyze an organizational information system need to elicit system requirements.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO6"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO2", 
                            CourseLearningOutcomeDesc = "Execute an appropriate SDLC process model.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO6"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO3", 
                            CourseLearningOutcomeDesc = "Given system requirements, design and implement an appropriate system architecture.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO6"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO4", 
                            CourseLearningOutcomeDesc = "Use source code management tools.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO6"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO5", 
                            CourseLearningOutcomeDesc = "Negotiate goals and conditions of satisfaction for a software project with clients and stakeholders.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO6"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO6", 
                            CourseLearningOutcomeDesc = "Utilize collaboration tools to effectively work together in a software development team.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO6"
                        }
                    }
                };
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances.Select(x => x.CourseLearningOutcomes.Take(6));
                // Assert that the CourseLearningOutcome property is not null inside CourseInstance (e.g. it includes CLO1)
                Assert.NotNull(result);
            }
        }
        
        [Fact]
        public void AddSevenCLOToCourseInstance()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "AddSixCLOToCourseInstance_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // Create a CourseInstance
                var courseInstance = new CourseInstance()
                {
                    CourseInstanceId = "4231570",
                    AcademicYearId = "2020-2021",
                    SemesterId = "Spring",
                    CourseId = "CIDM3320",
                    CourseLearningOutcomes = new List<CourseLearningOutcome> {
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO1", 
                            CourseLearningOutcomeDesc = "Analyze an organizational information system need to elicit system requirements.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO7"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO2", 
                            CourseLearningOutcomeDesc = "Execute an appropriate SDLC process model.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO7"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO3", 
                            CourseLearningOutcomeDesc = "Given system requirements, design and implement an appropriate system architecture.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO7"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO4", 
                            CourseLearningOutcomeDesc = "Use source code management tools.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO7"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO5", 
                            CourseLearningOutcomeDesc = "Negotiate goals and conditions of satisfaction for a software project with clients and stakeholders.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO7"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO6", 
                            CourseLearningOutcomeDesc = "Utilize collaboration tools to effectively work together in a software development team.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO7"
                        },
                        new CourseLearningOutcome() 
                        { 
                            CourseLearningOutcomeId = "CLO7", 
                            CourseLearningOutcomeDesc = "Implement a plan for the configuration, deployment and quality assurance of an information systems artifact.",
                            CourseInstanceId = "4231570",
                            CacStudentOutcomeId = "SO7"
                        }
                    }
                };
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances.Select(x => x.CourseLearningOutcomes.Take(7));
                // Assert that the CourseLearningOutcome property is not null inside CourseInstance (e.g. it includes CLO1)
                Assert.NotNull(result);
            }
        }
        private static WebApiDbContext CreateContext(DbContextOptions<WebApiDbContext> options) => new WebApiDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<CourseLearningOutcome>()
                .ToInMemoryQuery(() => context.CourseLearningOutcomes.Select(b => new CourseLearningOutcome { CourseLearningOutcomeId  = b.CourseLearningOutcomeId }));
            modelBuilder.Entity<CourseInstance>()
                .ToInMemoryQuery(() => context.CourseInstances.Select(b => new CourseInstance { CourseInstanceId  = b.CourseInstanceId }));
            modelBuilder.Entity<AcademicYear>()
                .ToInMemoryQuery(() => context.AcademicYears.Select(b => new AcademicYear { AcademicYearId = b.AcademicYearId }));
            modelBuilder.Entity<Course>()
                .ToInMemoryQuery(() => context.Courses.Select(b => new Course { CourseId = b.CourseId }));
            modelBuilder.Entity<Semester>()
                .ToInMemoryQuery(() => context.Semesters.Select(b => new Semester { SemesterId  = b.SemesterId }));
        });
    }
}