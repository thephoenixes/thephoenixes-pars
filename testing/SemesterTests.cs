using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;
using domain.SemesterAggregate;
using webapp.Data;

namespace testing
{
    public class SemesterTests
    {
        [Fact]
        public void Add_semester_to_database()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Add_semester_to_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                var semester = new Semester();

                semester.SemesterId = "Spring 2021";

                // Adds semester to the in-memory database
                context.Semesters.Add(semester);
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                // Verifying that the semester count is equal to 1 as expected,
                // and that the SemesterId was correctly saved in the database.
                Assert.Equal(1, context.Semesters.Count());
                Assert.Equal("Spring 2021", context.Semesters.Single().SemesterId);
            }
        }

        [Fact]
        public void Find_Semester()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_Semester")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding semesters to the database
                context.Semesters.Add(new Semester { SemesterId = "Spring 2021" });
                context.Semesters.Add(new Semester { SemesterId = "Fall 2021" });
                context.Semesters.Add(new Semester { SemesterId = "Spring 2022" });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.Semesters.Where(cust => cust.SemesterId.Contains("Spring 2021"));
                Assert.Equal(1, result.Count());
            }
        }

        [Fact]
        public void Return_All_Semesters()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Return_All_Semesters")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding semesters to the database
                context.Semesters.Add(new Semester { SemesterId = "Spring 2021" });
                context.Semesters.Add(new Semester { SemesterId = "Fall 2021" });
                context.Semesters.Add(new Semester { SemesterId = "Spring 2022" });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.Semesters;
                Assert.Equal(3, result.Count());
            }
        }

        [Fact]
        public void Delete_Semester()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Delete_Semester")
                .Options;

            using (var context = CreateContext(options))
            {
                // Add a record to be deleted
                context.Semesters.Add(new Semester { SemesterId = "Fall 2022" });
                context.SaveChanges();

                // Get the one specific semester
                var selectedSemester = context.Semesters.Where(s => s.SemesterId == "Fall 2022").FirstOrDefault();

                // Deletes selected semester
                context.Remove(selectedSemester);
                context.SaveChanges();

                // Makes sure the number of records is correct
                Assert.Equal(0, context.Semesters.Count());
            }
        }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<Semester>()
                .ToInMemoryQuery(() => context.Semesters.Select(b => new Semester { SemesterId  = b.SemesterId }));
        });
    }
}