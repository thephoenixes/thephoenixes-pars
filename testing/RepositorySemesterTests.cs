using Microsoft.EntityFrameworkCore;
using System.Linq;
using Xunit;
using repository;
using domain;
using domain.SemesterAggregate;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace testing
{
    public class RepositorySemesterTests
    {
        private ISemesterRepository semesterRepository;
        private IUnitOfWork semesterUnitOfWork;
        
        [Fact]
        public void GetAllSemestersTest()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "GetAllSemesters_database")
                .Options;

            using (var context = CreateContext(options))
            {
                // ARRANGE
                var semesterInMemoryDb = new List<Semester>
                {
                    new Semester() { SemesterId = "Fall 2019" },
                    new Semester() { SemesterId = "Spring 2020" },
                    new Semester() { SemesterId = "Fall 2020" },
                    new Semester() { SemesterId = "Spring 2021" }
                };
                context.SaveChanges();
                var repository = new Mock<ISemesterRepository>();

                // ACT
                repository.Setup(x => x.GetAll()).Returns(Task.FromResult<IEnumerable<Semester>>(semesterInMemoryDb));
                semesterRepository = repository.Object;
                var semesters = semesterRepository.GetAll();

                // ASSERT
                Assert.NotNull(semesters);
                Assert.Equal(4, semesters.Result.Count());
            }
        }

        [Fact]
        public void AddSemesterTest()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "AddSemester_database")
                .Options;

            using (var context = CreateContext(options))
            {
                // ARRANGE
                var semesterInMemoryDb = new List<Semester>
                {
                    new Semester() { SemesterId = "Fall 2019" },
                    new Semester() { SemesterId = "Spring 2020" },
                    new Semester() { SemesterId = "Fall 2020" },
                    new Semester() { SemesterId = "Spring 2021" }
                };
                context.SaveChanges();
                var repository = new Mock<ISemesterRepository>();
                var newSemester = new Semester() { SemesterId = "Fall 2021" };
 
                // ACT
                repository.Setup(x => x.Add(It.IsAny<Semester>())).Returns((Semester semester) =>
                {
                    semesterInMemoryDb.Add(semester);
                    return Task.FromResult(true);
                });
                semesterRepository = repository.Object;
                semesterRepository.Add(newSemester);

                // ASSERT
                Assert.Equal(5, semesterInMemoryDb.Count());
            }
        }

        [Fact]
        public void DeleteSemesterTest()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "DeleteSemester_database")
                .Options;

            using (var context = CreateContext(options))
            {
                // ARRANGE
                var semesterInMemoryDb = new List<Semester>
                {
                    new Semester() { SemesterId = "Fall 2019" },
                    new Semester() { SemesterId = "Spring 2020" },
                    new Semester() { SemesterId = "Fall 2020" },
                    new Semester() { SemesterId = "Spring 2021" }
                };
                context.SaveChanges();
                var repository = new Mock<ISemesterRepository>();
 
                // ACT
                repository.Setup(x => x.Delete(It.IsAny<Semester>())).Callback((Semester semester) =>
                {
                    semesterInMemoryDb.Remove(semester);
                });
                semesterRepository = repository.Object;
                semesterRepository.Delete(semesterInMemoryDb.Single(j => j.SemesterId == "Fall 2020"));

                // ASSERT
                Assert.Equal(3, semesterInMemoryDb.Count());
            }
        }


        [Fact]
        public void GetSemesterByIdTest()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "GetSemesterById_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // ARRANGE
                // Create in-memory database
                var semesterInMemoryDb = new List<Semester>
                {
                    new Semester() { SemesterId = "Fall 2019" },
                    new Semester() { SemesterId = "Spring 2020" },
                    new Semester() { SemesterId = "Fall 2020" },
                    new Semester() { SemesterId = "Spring 2021" }
                };
                context.SaveChanges();

                // Mock repository with Moq
                var repository = new Mock<ISemesterRepository>();

                // ACT
                // When calling GetById method defined in my IRepository contract, the Moq will try to find
                // matching element in the in-memory database and return it.
                repository.Setup(x => x.GetSemesterById(It.IsAny<string>())).Returns((string i) => semesterInMemoryDb.Single(s => s.SemesterId == i));
                // Create an object
                semesterRepository = repository.Object;

                // ASSERT
                // Look up that semester really exists
                var semesterThatExists = semesterRepository.GetSemesterById("Fall 2019");
                // Verify that the semester object is not null
                Assert.NotNull(semesterThatExists);
                // Verify that the object's semesterId matches what is expected
                Assert.Equal("Fall 2019", semesterThatExists.SemesterId);
            }
        }

        [Fact]
        public void RepositoryAndUnitOfWork_Test()
        {
            var options = new DbContextOptionsBuilder<WebApiDbContext>()
                .UseInMemoryDatabase(databaseName: "RepositoryAndUnitOfWork_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                // ARRANGE
                var myData = new List<Semester>
                {
                    new Semester() { SemesterId = "Fall 2019" },
                    new Semester() { SemesterId = "Spring 2020" },
                    new Semester() { SemesterId = "Fall 2020" },
                    new Semester() { SemesterId = "Spring 2021" }
                }.AsQueryable();
                context.SaveChanges();

                var repository = new Mock<ISemesterRepository>();
                var unitOfWork = new Mock<IUnitOfWork>();

                // ACT
                repository.Setup(x => x.GetSemesterById(It.IsAny<string>())).Returns((string i) => myData.Single(s => s.SemesterId == i));
                unitOfWork.Setup(u => u.Semesters).Returns(repository.Object);
                context.SaveChanges();

                semesterUnitOfWork = unitOfWork.Object;

                // ASSERT
                var semesterExists = semesterUnitOfWork.Semesters;
                Assert.NotNull(semesterExists);
            }
        }

        private static WebApiDbContext CreateContext(DbContextOptions<WebApiDbContext> options) => new WebApiDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<Semester>()
                .ToInMemoryQuery(() => context.Semesters.Select(b => new Semester { SemesterId  = b.SemesterId }));
        });
    }
}