﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using webapp.Data;
using Xunit;
using domain.AcademicYearAggregate;


namespace testing
{
    public class AcademicYearTests
    {
        private ApplicationDbContext _context;

        public AcademicYearTests()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "AcademicYearInMemoryDatabase")
                .Options;

            _context = CreateContext(options);
        }

        [Fact]
        public void Add_AcademicYears_to_database()
        {
            // variable to hold record count
            var currentRowCount = 0;

            // Add new academic year
            _context.AcademicYears.Add(new AcademicYear { AcademicYearId = "2019-2020" });
            _context.SaveChanges();

            // Get counter of current number of records.
            currentRowCount = _context.AcademicYears.Count();

            // Add another academic year
            _context.AcademicYears.Add(new AcademicYear { AcademicYearId = "2020-2021" });
            _context.SaveChanges();

            // Gets first record inserted
            var insertedRecord = _context.AcademicYears.Where(AY => AY.AcademicYearId == "2019-2020").FirstOrDefault();

            // Test that the record is found
            Xunit.Assert.NotNull(insertedRecord);
            // Test record count is correct
            Xunit.Assert.Equal(currentRowCount + 1, _context.AcademicYears.Count());
            // Make sure the record found is the right one
            Xunit.Assert.Equal("2019-2020", insertedRecord.AcademicYearId);
        }

        [Fact]
        public void Find_AcademicYear()
        {
            //Adding another Academic Year to the database
            _context.AcademicYears.Add(new AcademicYear { AcademicYearId = "2021-2022" });
            _context.SaveChanges();

            // Searches for the record.
            var result = _context.AcademicYears.Where(AY => AY.AcademicYearId.Contains("2021-2022"));

            // Test if any record is found
            Xunit.Assert.NotNull(result);
            // Check if only one record is found
            Xunit.Assert.Equal(1, result.Count());
        }

        [Fact]
        public void Return_All_AcademicYears()
        {
            // variable to hold record count
            var currentRowCount = _context.AcademicYears.Count();

            // Add new academic year
            _context.AcademicYears.Add(new AcademicYear { AcademicYearId = "2022-2023" });
            _context.SaveChanges();
            // Add new academic year
            _context.AcademicYears.Add(new AcademicYear { AcademicYearId = "2023-2024" });
            _context.SaveChanges();

            // Checks the count of all academic years
            Xunit.Assert.Equal(currentRowCount + 2, _context.AcademicYears.Count());
        }

        [Fact]
        public void Delete_AcademicYear()
        {
            // Get counter of current number of records.
            var currentRowCount = _context.AcademicYears.Count();

            // Add a record to be deleted.
            _context.AcademicYears.Add(new AcademicYear { AcademicYearId = "TestDelete" });
            _context.SaveChanges();

            // Get the one specific academic year
            var selectedAcademicYear = _context.AcademicYears.Where(AY => AY.AcademicYearId == "TestDelete").FirstOrDefault();

            // deletes the selectedc academic year
            _context.Remove(selectedAcademicYear);
            _context.SaveChanges();

            // Makes sure the number of records is correct
            Xunit.Assert.Equal(currentRowCount, _context.AcademicYears.Count());
        }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<AcademicYear>()
                .ToInMemoryQuery(() => context.AcademicYears.Select(b => new AcademicYear { AcademicYearId = b.AcademicYearId }));
        });
    }
}
