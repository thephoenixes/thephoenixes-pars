﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;
using domain.ProgramEducationalObjectiveAggregate;
using webapp.Data;

namespace testing
{
    public class ProgramEducationalObjectiveTests
    {
        [Fact]
        public void Add_ProgramEducationalObjective_to_database()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Add_ProgramEducationalObjective_to_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                var ProgramEducationalObjective = new ProgramEducationalObjective();

                ProgramEducationalObjective.ProgramEducationalObjectiveId = "PEO1";
                ProgramEducationalObjective.ProgramEducationalObjectiveDesc = "	(Applied Skills and Knowledge) Apply foundational knowledge and technical skills to work in a business environment.";

                context.ProgramEducationalObjectives.Add(ProgramEducationalObjective);
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                Assert.Equal(1, context.ProgramEducationalObjectives.Count());
                Assert.Equal("PEO1", context.ProgramEducationalObjectives.Single().ProgramEducationalObjectiveId);
            }
        }

        [Fact]
        public void Find_ProgramEducationalObjective()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_ProgramEducationalObjective")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding Program Educational Objectives to the database
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO1", ProgramEducationalObjectiveDesc= "(Applied Skills and Knowledge) Apply foundational knowledge and technical skills to work in a business environment." });
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO2", ProgramEducationalObjectiveDesc = "(Analysis and Design) Synthesize core knowledge of computer information systems to analyze, design, and implement solutions to real-wordl business problems." });
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO3", ProgramEducationalObjectiveDesc = "(Currency) Actively engage in life-long learning and professional development." });
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO4", ProgramEducationalObjectiveDesc = "(Teamwork) Work in teams to solve business problems by utilizing effective communication and collaboration skills." });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.ProgramEducationalObjectives.Where(cust => cust.ProgramEducationalObjectiveId.Contains("PEO3"));
                Assert.Equal(1, result.Count());
            }
        }

        [Fact]
        public void Return_All_ProgramEducationalObjective()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Return_All_ProgramEducationalObjectives")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding Program Educational Objectives to the database
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO1", ProgramEducationalObjectiveDesc = "(Applied Skills and Knowledge) Apply foundational knowledge and technical skills to work in a business environment." });
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO2", ProgramEducationalObjectiveDesc = "(Analysis and Design) Synthesize core knowledge of computer information systems to analyze, design, and implement solutions to real-wordl business problems." });
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO3", ProgramEducationalObjectiveDesc = "(Currency) Actively engage in life-long learning and professional development." });
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO4", ProgramEducationalObjectiveDesc = "(Teamwork) Work in teams to solve business problems by utilizing effective communication and collaboration skills." });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.ProgramEducationalObjectives;
                Assert.Equal(4, result.Count());
            }
        }

        [Fact]
        public void Update_ProgramEducationalObjective()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_ProgramEducationalObjective")
                .Options;

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var selectedOutcome = context.ProgramEducationalObjectives.Where(SO => SO.ProgramEducationalObjectiveId == "PEO2").FirstOrDefault();
                selectedOutcome.ProgramEducationalObjectiveDesc = "Description Changed";

                context.SaveChanges();

                var result = context.ProgramEducationalObjectives.Where(SO => SO.ProgramEducationalObjectiveId == "PEO2").FirstOrDefault();
                Assert.NotNull(result);

                Assert.Equal("Description Changed", result.ProgramEducationalObjectiveDesc);
            }
        }

        [Fact]
        public void Delete_ProgramEducationalObjective()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_ProgramEducationalObjective")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                var result = context.ProgramEducationalObjectives.Where(cust => cust.ProgramEducationalObjectiveId == "PEO4").FirstOrDefault();
                context.Remove(result);
                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                Assert.Equal(3, context.ProgramEducationalObjectives.Count());
            }
        }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<ProgramEducationalObjective>()
                .ToInMemoryQuery(() => context.ProgramEducationalObjectives.Select(b => new ProgramEducationalObjective { ProgramEducationalObjectiveId = b.ProgramEducationalObjectiveId }));
        });
    }
}
