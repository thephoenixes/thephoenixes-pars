using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;
using domain.CourseInstanceAggregate;
using webapp.Data;

namespace testing
{
    public class CourseInstanceTests
    {
        [Fact]
        public void Add_CourseInstance_to_database()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Add_CourseInstance_to_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                var courseInstance = new CourseInstance();

                courseInstance.CourseInstanceId = "1";

                context.CourseInstances.Add(courseInstance);
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                Assert.Equal(1, context.CourseInstances.Count());
                Assert.Equal("1", context.CourseInstances.Single().CourseInstanceId);
            }
        }

        [Fact]
        public void Find_CLO()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_CourseInstance")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding Delivery persons to the database
                context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "1" });
                context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "2" });
                context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "3" });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances.Where(ci => ci.CourseInstanceId == "2");
                Assert.Equal(1, result.Count());
            }
        }

        [Fact]
        public void Return_All_CourseInstance()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Return_All_CourseInstance")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding Delivery persons to the database
                context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "1" });
                context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "2" });
                context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "3" });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.CourseInstances;
                Assert.Equal(3, result.Count());
            }
        }

         [Fact]
            public void Delete_CourseInstance()
            {
                var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseInMemoryDatabase(databaseName: "Delete_CourseInstance")
                    .Options;

                using (var context = CreateContext(options))
                {
                    // Add a record to be deleted.
                    context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "1" });
                    context.SaveChanges();

                    // Get the one specific CourseInstance
                    var selectedCourseInstance = context.CourseInstances.Where(s => s.CourseInstanceId == "1").FirstOrDefault();

                    // Deletes selected CourseInstance
                    context.Remove(selectedCourseInstance);
                    context.SaveChanges();

                    // Makes sure the number of records is correct
                    Assert.Equal(0, context.CourseInstances.Count());
                }
            }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<CourseInstance>()
                .ToInMemoryQuery(() => context.CourseInstances.Select(b => new CourseInstance { CourseInstanceId = b.CourseInstanceId }));
        });
    }
}