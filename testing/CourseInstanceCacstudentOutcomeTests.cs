using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;
using domain.CourseInstanceCacstudentOutcomeAggregate;
using domain.CacstudentOutcomeAggregate;
using domain.AcademicYearAggregate;
using domain.CourseAggregate;
using domain.SemesterAggregate;
using domain.CourseInstanceAggregate;
using webapp.Data;
using Xunit.Priority;

namespace testing
{
    [TestCaseOrderer("CourseInstanceCacstudentOutcomeTests", "CourseInstanceCacstudentOutcomeTests")]
    public class CourseInstanceCacstudentOutcomeTests
    {
        private ApplicationDbContext _context;

        public CourseInstanceCacstudentOutcomeTests()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "CourseInstanceCacstudentOutcome")
                .Options;

            _context = CreateContext(options);
        }

        [Fact, Priority(0)]
        public void Add_CourseInstanceCacstudentOutcome_to_database()
        {
            var courseInstance1 = new CourseInstance() {
                CourseInstanceId = "CIDM1234-01"
            };
            courseInstance1.AcademicYear = new AcademicYear { AcademicYearId = "2020-2021" };
            courseInstance1.Semester = new Semester { SemesterId = "Spring 2021" };
            courseInstance1.Course = new Course { CourseId = "CIDM1234", CourseName = "Some course name" };

            _context.CourseInstances.Add(courseInstance1);

            var cacstudentOutcome1 = new CacstudentOutcome();
            cacstudentOutcome1.CacStudentOutcomeId = "SO1";
            cacstudentOutcome1.CacStudentOutcomeDesc = "An ability to analyze a complex computing problem and to apply principles of computing and other releant disciplines to identify solutions.";

            _context.CacstudentOutcomes.Add(cacstudentOutcome1);

            _context.SaveChanges();

            var CourseInstanceCacstudentOutcome = new CourseInstanceCacstudentOutcome();

            CourseInstanceCacstudentOutcome.CourseInstanceId = courseInstance1.CourseInstanceId;
            CourseInstanceCacstudentOutcome.CacStudentOutcomeId = cacstudentOutcome1.CacStudentOutcomeId;

            _context.CourseInstanceCacstudentOutcomes.Add(CourseInstanceCacstudentOutcome);
            _context.SaveChanges();

            Assert.Equal(1, _context.CourseInstanceCacstudentOutcomes.Count());
        }

        [Fact, Priority(1)]
        public void Delete_CourseInstanceCacstudentOutcome()
        {
            // Get counter of current number of records.
            var currentRowCount = _context.CourseInstanceCacstudentOutcomes.Count();
            Xunit.Assert.True(currentRowCount >= 1, "No records found to be deleted.");

            // Get the one specific academic year
            var selectedCourseInstanceCacstudentOutcomes = _context.CourseInstanceCacstudentOutcomes.FirstOrDefault();

            // deletes the selectedc academic year
            _context.Remove(selectedCourseInstanceCacstudentOutcomes);
            _context.SaveChanges();

            // Makes sure the number of records is correct
            Xunit.Assert.Equal(currentRowCount - 1, _context.CourseInstanceCacstudentOutcomes.Count());
        }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<CourseInstanceCacstudentOutcome>()
                .ToInMemoryQuery(() => context.CourseInstanceCacstudentOutcomes.Select(b => new CourseInstanceCacstudentOutcome { CourseInstanceId = b.CourseInstanceId }));
        });
    }
}
