using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using webapp.Data;
using Xunit;
using domain.CourseAggregate;


namespace testing
{
    public class CourseTests
    {
        private ApplicationDbContext _context;

        public CourseTests()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "CourseInMemoryDatabase")
                .Options;

            _context = CreateContext(options);
        }

        [Fact]
        public void Add_Courses_to_database()
        {
            // variable to hold record count
            var currentRowCount = 0;

            // Add new course
            _context.Courses.Add(new Course { CourseId = "CIDM 4390" });
            _context.SaveChanges();

            // Get counter of current number of records.
            currentRowCount = _context.Courses.Count();

            // Add another course
            _context.Courses.Add(new Course { CourseId = "CIDM 4372" });
            _context.SaveChanges();

            // Gets first record inserted
            var insertedRecord = _context.Courses.Where(c => c.CourseId == "CIDM 4390").FirstOrDefault();

            // Test that the record is found
            Xunit.Assert.NotNull(insertedRecord);
            // Test record count is correct
            Xunit.Assert.Equal(currentRowCount + 1, _context.Courses.Count());
            // Make sure the record found is the right one
            Xunit.Assert.Equal("CIDM 4390", insertedRecord.CourseId);
        }

         [Fact]
        public void Find_Course()
        {
            //Adding another Course to the database
            _context.Courses.Add(new Course { CourseId = "CIDM 3312" });
            _context.SaveChanges();

            // Searches for the record.
            var result = _context.Courses.Where(c => c.CourseId.Contains("CIDM 3312"));

            // Test if any record is found
            Xunit.Assert.NotNull(result);
            // Check if only one record is found
            Xunit.Assert.Equal(1, result.Count());
        }

        [Fact]
        public void Return_All_Courses()
        {
            // variable to hold record count
            var currentRowCount = _context.Courses.Count();

            // Add new Course
            _context.Courses.Add(new Course { CourseId = "CIDM 3316" });
            _context.SaveChanges();
            // Add new Course
            _context.Courses.Add(new Course { CourseId = "CIDM 3385" });
            _context.SaveChanges();

            // Checks the count of all Courses
            Xunit.Assert.Equal(currentRowCount + 2, _context.Courses.Count());
        }

        [Fact]
        public void Delete_Course()
        {
            // Get counter of current number of records.
            var currentRowCount = _context.Courses.Count();

            // Add a record to be deleted.
            _context.Courses.Add(new Course { CourseId = "TestDelete" });
            _context.SaveChanges();

            // Get the one specific Course
            var selectedCourse = _context.Courses.Where(c => c.CourseId == "TestDelete").FirstOrDefault();

            // deletes the selected Course
            _context.Remove(selectedCourse);
            _context.SaveChanges();

            // Makes sure the number of records is correct
            Xunit.Assert.Equal(currentRowCount, _context.Courses.Count());
        }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<Course>()
                .ToInMemoryQuery(() => context.Courses.Select(b => new Course { CourseId = b.CourseId }));
        }); 
    }
}