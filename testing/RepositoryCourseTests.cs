﻿using Microsoft.EntityFrameworkCore;
using Xunit;
using repository;
using domain.CourseAggregate;
using Moq;
using webapp.Data;
using System.Linq;
using System.Collections.Generic;

namespace testing
{
    public class RepositoryCourseTests
    {
        //private ApplicationDbContext _context;

        [Fact]
        public void CreateCourse_Test()
        {
            // In memory Database used for the test
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Repository_database")
                .Options;

            // Setting up mock variables used in test
            var mockSet = new Mock<DbSet<Course>>();
            var mockContext = new Mock<ApplicationDbContext>(options);
            mockContext.Setup(m => m.Courses).Returns(mockSet.Object);
            var repo = new CourseRepository(mockContext.Object);

            //Creates and Saves the course to the database
            repo.CreateCourse(new Course { CourseId = "CIDM 4390", CourseName = "Software Systems Development" });

            //Verify that Course was created only once.
            mockSet.Verify(m => m.Add(It.IsAny<Course>()), Times.Once());

            //Verify that Course was save only once.
            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [Fact]
        public void GetAllCourses_Test()
        {
            // In memory Database used for the test
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Repository_database")
                .Options;

            // Temporary data used in the tests
            var data = new List<Course>
            {
                new Course { CourseId = "CIDM 4390", CourseName = "Software Systems Development" },
                new Course { CourseId = "CIDM 4372", CourseName = "Information Visualization" },
                new Course { CourseId = "CIDM 3385", CourseName = "Network Security" }
            }.AsQueryable();

            // Setting up mock variables used in test
            var mockSet = new Mock<DbSet<Course>>();
            mockSet.As<IQueryable<Course>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Course>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Course>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Course>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<ApplicationDbContext>(options);
            mockContext.Setup(m => m.Courses).Returns(mockSet.Object);
            var repo = new CourseRepository(mockContext.Object);

            // Get all courses
            var allCourses = repo.GetAllCourses();

            // Assert/Verify results
            Assert.Equal(3, allCourses.Count);
            Assert.Equal("CIDM 4390", allCourses[0].CourseId);
            Assert.Equal("CIDM 4372", allCourses[1].CourseId);
            Assert.Equal("CIDM 3385", allCourses[2].CourseId);
        }

        [Fact]
        public void GetCourseById_Test()
        {
            // In memory Database used for the test
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Repository_database")
                .Options;

            // Temporary data used in the tests
            var data = new List<Course>
            {
                new Course { CourseId = "CIDM 4390", CourseName = "Software Systems Development" },
                new Course { CourseId = "CIDM 4372", CourseName = "Information Visualization" },
                new Course { CourseId = "CIDM 3385", CourseName = "Network Security" }
            }.AsQueryable();

            // Setting up mock variables used in test
            var mockSet = new Mock<DbSet<Course>>();
            mockSet.As<IQueryable<Course>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Course>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Course>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Course>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<ApplicationDbContext>(options);
            mockContext.Setup(m => m.Courses).Returns(mockSet.Object);
            var repo = new CourseRepository(mockContext.Object);

            // Get a specific course Id = "CIDM 4390"
            var selectedCourse = repo.GetCourseById("CIDM 4390");

            // Assert/Verify results
            Assert.NotNull(selectedCourse);
            Assert.Equal("Software Systems Development", selectedCourse.CourseName);
        }
    }
}
