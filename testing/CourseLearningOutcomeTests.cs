using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;
using domain.CourseLearningOutcomeAggregate;
using webapp.Data;

namespace testing
{
    public class CourseLearningOutcomeTests
    {
        [Fact]
        public void Add_CLO_to_database()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Add_CLO_to_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                var courseLearningOutcome = new CourseLearningOutcome();

                courseLearningOutcome.CourseLearningOutcomeId = "CLO1";

                context.CourseLearningOutcomes.Add(courseLearningOutcome);
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                // Verifying that the CLO count is equal to 1 as expected,
                // and that the CourseLearningOutcomeId was correctly saved in the in-memory database.
                Assert.Equal(1, context.CourseLearningOutcomes.Count());
                Assert.Equal("CLO1", context.CourseLearningOutcomes.Single().CourseLearningOutcomeId);
            }
        }

        [Fact]
        public void Find_CLO()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_CLO")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding CLOs to the database
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO1" });
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO2" });
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO3" });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.CourseLearningOutcomes.Where(clo => clo.CourseLearningOutcomeId.Contains("CLO2"));
                Assert.Equal(1, result.Count());
            }
        }

        [Fact]
        public void Return_All_CLO()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Return_All_CLO")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding CLOs to the database
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO1" });
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO2" });
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO3" });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.CourseLearningOutcomes;
                Assert.Equal(3, result.Count());
            }
        }

        [Fact]
        public void Delete_CLO()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Delete_CLO")
                .Options;

            using (var context = CreateContext(options))
            {
                // Add a record to be deleted.
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO4" });
                context.SaveChanges();

                // Get the one specific CLO
                var selectedCLO = context.CourseLearningOutcomes.Where(s => s.CourseLearningOutcomeId == "CLO4").FirstOrDefault();

                // Deletes selected CLO
                context.Remove(selectedCLO);
                context.SaveChanges();

                // Makes sure the number of records is correct
                Assert.Equal(0, context.CourseLearningOutcomes.Count());
            }
        }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<CourseLearningOutcome>()
                .ToInMemoryQuery(() => context.CourseLearningOutcomes.Select(b => new CourseLearningOutcome { CourseLearningOutcomeId  = b.CourseLearningOutcomeId }));
        });
    }
}