﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;
using domain.CacstudentOutcomeAggregate;
using webapp.Data;

namespace testing
{
    public class StudentOutcomeTests
    {
        [Fact]
        public void Add_StudentOutcome_to_database()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Add_StudentOutcome_to_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                var CacstudentOutcome = new CacstudentOutcome();

                CacstudentOutcome.CacStudentOutcomeId = "SO1";
                CacstudentOutcome.CacStudentOutcomeDesc = "An ability to analyze a complex computing problem and to apply principles of computing and other releant disciplines to identify solutions.";

                context.CacstudentOutcomes.Add(CacstudentOutcome);
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                Assert.Equal(1, context.CacstudentOutcomes.Count());
                Assert.Equal("SO1", context.CacstudentOutcomes.Single().CacStudentOutcomeId);
            }
        }

        [Fact]
        public void Find_StudentOutcome()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_StudentOutcome")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding student Outcomes to the database
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO1", CacStudentOutcomeDesc= "An ability to analyze a complex computing problem and to apply principles of computing and other releant disciplines to identify solutions." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO2", CacStudentOutcomeDesc = "An ability to design, implement, and evaluate a computing-based solution to meet a given set of computing requirements in the context of the program’s discipline." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO3", CacStudentOutcomeDesc = "An ability to communicate effectively in a variety of professional contexts." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO4", CacStudentOutcomeDesc = "An ability to recognize professional responsibilities and make informed judgments in computing practice based on legal and ethical principles." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO5", CacStudentOutcomeDesc = "An ability to function effectively as a member or leader of a team engaged in activities appropriate to the program’s discipline." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO6", CacStudentOutcomeDesc = "An ability to support the delivery, use, and management of information systems within an information systems environment." });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.CacstudentOutcomes.Where(cust => cust.CacStudentOutcomeId.Contains("SO5"));
                Assert.Equal(1, result.Count());
            }
        }

        [Fact]
        public void Return_All_StudentOutcome()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Return_All_StudentOutcomes")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding student Outcomes to the database
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO1", CacStudentOutcomeDesc = "An ability to analyze a complex computing problem and to apply principles of computing and other releant disciplines to identify solutions." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO2", CacStudentOutcomeDesc = "An ability to design, implement, and evaluate a computing-based solution to meet a given set of computing requirements in the context of the program’s discipline." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO3", CacStudentOutcomeDesc = "An ability to communicate effectively in a variety of professional contexts." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO4", CacStudentOutcomeDesc = "An ability to recognize professional responsibilities and make informed judgments in computing practice based on legal and ethical principles." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO5", CacStudentOutcomeDesc = "An ability to function effectively as a member or leader of a team engaged in activities appropriate to the program’s discipline." });
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO6", CacStudentOutcomeDesc = "An ability to support the delivery, use, and management of information systems within an information systems environment." });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.CacstudentOutcomes;
                Assert.Equal(6, result.Count());
            }
        }

        [Fact]
        public void Update_StudentOutcome()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_StudentOutcome")
                .Options;

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var selectedOutcome = context.CacstudentOutcomes.Where(SO => SO.CacStudentOutcomeId == "SO4").FirstOrDefault();
                selectedOutcome.CacStudentOutcomeDesc = "Description Changed";

                context.SaveChanges();

                var result = context.CacstudentOutcomes.Where(SO => SO.CacStudentOutcomeId == "SO4").FirstOrDefault();
                Assert.NotNull(result);

                Assert.Equal("Description Changed", result.CacStudentOutcomeDesc);
            }
        }

        [Fact]
        public void Delete_StudentOutcome()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_StudentOutcome")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                var result = context.CacstudentOutcomes.Where(cust => cust.CacStudentOutcomeId == "SO1").FirstOrDefault();
                context.Remove(result);
                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                Assert.Equal(5, context.CacstudentOutcomes.Count());
            }
        }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<CacstudentOutcome>()
                .ToInMemoryQuery(() => context.CacstudentOutcomes.Select(b => new CacstudentOutcome { CacStudentOutcomeId = b.CacStudentOutcomeId }));
        });
    }
}
