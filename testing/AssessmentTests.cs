using System.Linq;
using Microsoft.EntityFrameworkCore;
using Xunit;
using domain.AssessmentAggregate;
using webapp.Data;

namespace testing
{
    public class AssessmentTests
    {
        [Fact]
        public void Add_assessment_to_database()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Add_assessment_to_database")
                .Options;

            // Run the test against one instance of the context
            using (var context = CreateContext(options))
            {
                var assessment = new Assessment();

                assessment.AssessmentId = 1;

                context.Assessments.Add(assessment);
                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database
            using (var context = CreateContext(options))
            {
                Assert.Equal(1, context.Assessments.Count());
                Assert.Equal(1, context.Assessments.Single().AssessmentId);
            }
        }

        [Fact]
        public void Find_Assessment()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Find_Assessment")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding Assessments to the database
                context.Assessments.Add(new Assessment { AssessmentId = 1 });
                context.Assessments.Add(new Assessment { AssessmentId = 2 });
                context.Assessments.Add(new Assessment { AssessmentId = 3 });
                context.Assessments.Add(new Assessment { AssessmentId = 4 });

                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.Assessments.Where(assess => assess.AssessmentId.ToString().Contains("2"));
                Assert.Equal(1, result.Count());
            }
        }

        [Fact]
        public void Return_All_Assessment()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Return_All_Assessments")
                .Options;

            // Insert seed data into the database using one instance of the context
            using (var context = CreateContext(options))
            {
                //Adding Assessments to the database
                context.Assessments.Add(new Assessment { AssessmentId = 1 });
                context.Assessments.Add(new Assessment { AssessmentId = 2 });
                context.Assessments.Add(new Assessment { AssessmentId = 3 });
                context.Assessments.Add(new Assessment { AssessmentId = 4 });


                context.SaveChanges();
            }

            // Use a clean instance of the context to run the test
            using (var context = CreateContext(options))
            {
                var result = context.Assessments;
                Assert.Equal(4, result.Count());
            }
        }


        [Fact]
        public void Delete_Assessment()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "Delete_Assessment")
                .Options;

            using (var context = CreateContext(options))
            {
                // Add a record to be deleted
                context.Assessments.Add(new Assessment { AssessmentId = 1 });
                context.SaveChanges();

                // Get the one specific assessment
                var selectedAssessment = context.Assessments.Where(s => s.AssessmentId == 1).FirstOrDefault();

                // Deletes selected semester
                context.Remove(selectedAssessment);
                context.SaveChanges();

                // Makes sure the number of records is correct
                Assert.Equal(0, context.Assessments.Count());
            }
        }

        private static ApplicationDbContext CreateContext(DbContextOptions<ApplicationDbContext> options) => new ApplicationDbContext(options, (context, modelBuilder) =>
        {
            modelBuilder.Entity<Assessment>()
                .ToInMemoryQuery(() => context.Assessments.Select(b => new Assessment { AssessmentId  = b.AssessmentId }));
        });
    }
}