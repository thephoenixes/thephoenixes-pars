# PARS by The Phoenixes - 2021SP CIDM 4390-01

# Team Members
* SCRUM Master: **Gerardo Herrera Gonzalez**</br>
    * Responsible for hosting the daily scrums and assist the team with the product backlog.
    * Remove impediments.
    * Teach Scrum practices and principles to the team.
    * Planning every Sprint and assign subtasks.
    * Responsible for making sure that the code is checked in and all the PBIs are ready/done.
    * Responsible for completing designated subtasks during a given Sprint.
* Lead Developer: **Jonathan Lofgren**</br>
    * Responsible for completing designated subtasks during a given Sprint.
    * Responsible for collaborating with the team to identify technical issues.
    * Responsible for managing technical aspects of the software development flow in the team.
* UI Lead: **Rachael Slavik**</br>
    * Responsible for the UI/UX of the web application.
    * Responsible for completing designated subtasks during a given Sprint.
* Pipeline Lead: **Mary Browning**</br>
    * Responsible for leading deployment.
    * Responsible for completing designated subtasks during a given Sprint.
* Even though each of the team members had specific duties assigned depending on our roles, each of us were in charge of completing our designated PBI subtasks which are documented in Jira and listed in this document.

# Repository Structure
* [domain](https://bitbucket.org/thephoenixes/thephoenixes-pars/src/master/domain/)
* [repository](https://bitbucket.org/thephoenixes/thephoenixes-pars/src/master/repository/)
* [testing](https://bitbucket.org/thephoenixes/thephoenixes-pars/src/master/testing/)
* [webapi](https://bitbucket.org/thephoenixes/thephoenixes-pars/src/master/webapi/)
* [webapp](https://bitbucket.org/thephoenixes/thephoenixes-pars/src/master/webapp/)
* [Documents](https://bitbucket.org/thephoenixes/thephoenixes-pars/src/master/Documents/)
    * **Evidence Portfolio**: Word document including the evidence of our System and Analysis Design Diagrams, Scrum Process, Sprints, and Daily Scrums.
    * **Scrum Checklists**: Excel document that includes the checklists for every Sprint.
    * **SOLID Principles**: Word document explaining how the SOLID principles apply to our project code.

# Project Details
Our project consists of PARS, a system that allows Professors and ABET Evaluators keep track of criterion 4, continuous improvement. The system documents the processes for regularly assessing and evaluating the extent to which the student outcomes are being attained and it also describes how the results of these processes are utilized to affect continuous improvement of the program.

# Main Features
The PARS is protected by authentication in which users are asked for their login credentials to access the system. Each user is assigned a system role (Professor or ABET Evaluator), and if a new role surges, it can be added to the system by the administrator. The administrator has access to every feature in the system. The professors are able to create a Faculty Qualitative Assessment Report and Direct Course Embedded Assessment Reports, while the ABET evaluators are able to review them from their accounts.

# Daily Scrums Recordings
The recordings of the daily scrums can be found at: https://drive.google.com/drive/folders/1N12sTRRQAmo9ZrsT3N9D7F1K2hrQhv3k?usp=sharing

# Evidence of Sprint Planning
At the beginning of each new sprint, the Scrum team discussed the sprint goal for the upcoming sprint and based on that we created the user stories for that sprint and the sub-tasks for each of the team members. We played the Planning Poker game to prioritize and assign story points to each PBI. Finally, we allocated the sub-tasks to each of us depending in our roles. However, if one of us had an impediment during the sprint with one of the subtasks, we tried to go over it as a team.

# Evidence of Sprint Review
Each of the sprint reviews were conducted with Dr. Babb and the Scrum team every two Tuesdays at 8:30 pm via Zoom. During the sprint review, the team shared what was accomplished during the Sprint and a quick demonstration of the selected user stories for the sprint goal.

# Evidence of Sprint Retrospective
After each sprint review, the Scrum team had to answer three questions as a reflection of what happened during the sprint. The prompts that were addressed during each retrospective were:
* What worked well this sprint that we want to continue doing?
* What did not work well this sprint that we should stop doing?
* What should we start doing or improve?

# Definition of Ready
We utilized our definition of ready in each sprint planning event. It can be reviewed at https://drive.google.com/file/d/1GvabHGnktgRmdQ_-jiIFJijyou9nnymR/view

# Definition of Done
We utilized our definition of done at the end of each sprint. For a PBI to be done, the code had to be reviewed, completed, and zero known defects. It can reviewed at https://drive.google.com/file/d/16gk_42ADsedsVOizWmPbXpHtT2JYJRIh/view

# Sprints
## **Sprint 3**
### Sprint Goal
Create a Domain Class Library and Authentication and Authorization: I want different users (Admin, Professors, ABET Program Evaluators) to be able to log in to an account and to control what parts of the application they can view/edit.
### Sprint PBIs
![picture](img/Sprint3-PBIs.png)
### Subtasks
![picture](img/Sprint3-Subtasks.png)
### Daily Scrums
* Daily Scrum 1: 03/14/2021
* Daily Scrum 2: 03/15/2021
* Daily Scrum 3: 03/21/2021
* Daily Scrum 4: Springbreak
* Daily Scrum 5: Springbreak
* Daily Scrum 6: Springbreak
* Video recordings: https://drive.google.com/drive/folders/1N12sTRRQAmo9ZrsT3N9D7F1K2hrQhv3k?usp=sharing
### Burndown Chart
![picture](img/Sprint3-Burndown.png)
## **Sprint 4**
### Sprint Goal
Scaffold up the program educational objectives (PEO) and student outcomes (SO) and unit test class library using Xunit.
### Sprint PBIs
![picture](img/Sprint4-PBIs.png)
### Subtasks
![picture](img/Sprint4-Subtasks.png)
### Daily Scrums
* Daily Scrum 1: 03/24/2021
* Daily Scrum 2: 03/28/2021
* Daily Scrum 3: 03/31/2021
* Daily Scrum 4: 04/02/2021
* Daily Scrum 5: 04/05/2021
* Video recordings: https://drive.google.com/drive/folders/1N12sTRRQAmo9ZrsT3N9D7F1K2hrQhv3k?usp=sharing
### Burndown Chart
![picture](img/Sprint4-Burndown.png)
## **Sprint 5**
### Sprint Goal
Refactor code to implement the Repository Pattern and be able to assign Course Learning Outcomes to Courses.
### Sprint PBIs
![picture](img/Sprint5-PBIs.png)
### Subtasks
![picture](img/Sprint5-Subtasks.png)
### Daily Scrums
* Daily Scrum 1: 04/07/2021
* Daily Scrum 2: 04/10/2021
* Daily Scrum 3: 04/12/2021
* Daily Scrum 4: 04/16/2021
* Daily Scrum 5: 04/19/2021
* Video recordings: https://drive.google.com/drive/folders/1N12sTRRQAmo9ZrsT3N9D7F1K2hrQhv3k?usp=sharing
### Burndown Chart
![picture](img/Sprint5-Burndown.png)
## **Sprint 6**
### Sprint Goal
Develop Faculty Qualitative Assessment Report and Direct Course Embedded Assessment Report
### Sprint PBIs
![picture](img/Sprint6-PBIs.png)
### Subtasks
![picture](img/Sprint6-Subtasks.png)
### Daily Scrums
* Daily Scrum 1: 04/23/2021
* Daily Scrum 2: 04/25/2021
* Daily Scrum 3: 04/28/2021
* Daily Scrum 4: 05/02/2021
* Daily Scrum 5: 05/03/2021
* Video recordings: https://drive.google.com/drive/folders/1N12sTRRQAmo9ZrsT3N9D7F1K2hrQhv3k?usp=sharing
### Burndown Chart
![picture](img/Sprint6-Burndown.png)

# Jira and Bitbucket Ownership Transfer Evidence
Transfer of the Bitbucket and [Jira](https://gherreragonzalez1.atlassian.net/secure/RapidBoard.jspa?rapidView=3&projectKey=CIDMP) ownership was done on May 3, 2021.</br>
* Link to Jira project: https://gherreragonzalez1.atlassian.net/secure/RapidBoard.jspa?rapidView=3&projectKey=CIDMP </br>
* Link to Bitbucket repository: https://bitbucket.org/thephoenixes/thephoenixes-pars/src/master/
![picture](img/JiraOwnership.png)
![picture](img/BitbucketOwnership.png)

# How to Run the Project
1. Clone the repository and restore it ```dotnet restore```.
2. Run ```dotnet ef database update``` inside of the pars/webapp folder to obtain the database file (app.db).
3. To run the webapp project: Run ```dotnet run``` inside of pars/webapp folder.
4. To run the testing project: Run ```dotnet test``` inside of the pars/testing folder.
5. To run the webapi project: Run ```dotnet run``` inside of the pars/webapi folder.

# Log in credentials (For demonstration purposes)
* Admin
    * Username: ```admin```
    * Password: ```123Pa$$word```
* Professor
    * Username: ```professor```
    * Password: ```123Pa$$word```
* Evaluator
    * Username: ```evaluator```
    * Password: ```123Pa$$word```

# Screenshots of PARS
## Homepage (Admin)
![picture](img/Screenshots/HomepageAdmin.png)
## Homepage (Professor)
![picture](img/Screenshots/HomepageProfessor.png)
## Homepage (Evaluator)
![picture](img/Screenshots/HomepageEvaluator.png)
## Faculty Qualitative Assessment Reports Page (Admin)
![picture](img/Screenshots/FacultyQualitativeAssessmentReportPageAdmin.png)
## Assessment Reports Page (Admin)
![picture](img/Screenshots/AssessmentReportPageAdmin.png)
## Academic Years Page (Admin)
![picture](img/Screenshots/AcademicYearsPageAdmin.png)
## Semesters Page (Admin)
![picture](img/Screenshots/SemestersPageAdmin.png)
## Courses Page (Admin)
![picture](img/Screenshots/CoursesPageAdmin.png)
## Course Instances Page (Admin)
![picture](img/Screenshots/CourseInstancesPageAdmin.png)
## Course Learning Outcomes Page (Admin)
![picture](img/Screenshots/CourseLearningOutcomePageAdmin.png)
## Student Outcomes Page (Admin)
![picture](img/Screenshots/StudentOutcomesPageAdmin.png)
## Program Educational Objectives Page (Admin)
![picture](img/Screenshots/PEOPageAdmin.png)
## User Roles Page (Admin)
![picture](img/Screenshots/UserRolesPageAdmin.png)
## Manage User Roles Page (Admin)
![picture](img/Screenshots/ManageUserRolesPageAdmin.png)
## Manage User Accounts Page (Admin)
![picture](img/Screenshots/ManageUserAccountsPageAdmin.png)
## Role Manager Page (Admin)
![picture](img/Screenshots/RoleManagerPageAdmin.png)

## Thank you! - The Phoenixes
![picture](img/Logo.png)
