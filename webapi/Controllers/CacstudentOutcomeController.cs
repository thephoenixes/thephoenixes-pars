using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using domain;
using domain.CacstudentOutcomeAggregate;
using repository;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CacstudentOutcomeController : ControllerBase
    {
        private readonly ILogger<CacstudentOutcomeController> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public CacstudentOutcomeController(ILogger<CacstudentOutcomeController> logger,
                                      IUnitOfWork unitOfWork)

        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        // GET: api/<CacstudentOutcome>
        [HttpGet]
        public async Task<IEnumerable<CacstudentOutcome>> Get()
        {
            _logger.LogInformation("Student Outcomes Called");
            return await _unitOfWork.CacstudentOutcomes.GetAll();
        }

        // GET api/<CacstudentOutcome>/SO1
        [HttpGet("{id}")]
        public async Task<CacstudentOutcome> Get(string id)
        {
            _logger.LogInformation("Student Outcome Called by ID");
            return await _unitOfWork.CacstudentOutcomes.Get(id);

        }

        // POST api/<CacstudentOutcome>
        [HttpPost]
        public IActionResult Post()
        {
            var SO1 = new CacstudentOutcome
            {
                CacStudentOutcomeId = "SO1",
                CacStudentOutcomeDesc = "An ability to analyze a complex computing problem and to apply principles of computing and other relevant disciplines to identify solutions."
            };
            _unitOfWork.CacstudentOutcomes.Add(SO1);

            var SO2 = new CacstudentOutcome
            {
                CacStudentOutcomeId = "SO2",
                CacStudentOutcomeDesc = "An ability to design, implement, and evaluate a computing-based solution to meet a given set of computing requirements in the context of the program's discipline."
            };
            _unitOfWork.CacstudentOutcomes.Add(SO2);

            var SO3 = new CacstudentOutcome
            {
                CacStudentOutcomeId = "SO3",
                CacStudentOutcomeDesc = "An ability to communicate effectively in a variety of professional contexts."
            };
            _unitOfWork.CacstudentOutcomes.Add(SO3);

            var SO4 = new CacstudentOutcome
            {
                CacStudentOutcomeId = "SO4",
                CacStudentOutcomeDesc = "An ability to recognize professional responsibilities and make informed judgments in computing practice based on legal and ethical principles."
            };
            _unitOfWork.CacstudentOutcomes.Add(SO4);

            var SO5 = new CacstudentOutcome
            {
                CacStudentOutcomeId = "SO5",
                CacStudentOutcomeDesc = "An ability to function effectively as a member or leader of a team engaged in activities appropriate to the program's discipline."
            };
            _unitOfWork.CacstudentOutcomes.Add(SO5);

            var SO6 = new CacstudentOutcome
            {
                CacStudentOutcomeId = "SO6",
                CacStudentOutcomeDesc = "An ability to support the delivery, use, and management of information systems within an information systems environment."
            };
            _unitOfWork.CacstudentOutcomes.Add(SO6);

            _logger.LogInformation("Student Outcomes Posted");
            _unitOfWork.Complete();
            return Ok();
        }

        // PUT api/<CacstudentOutcome>/SO1
        [HttpPut("{id}")]
        public IActionResult Put(string id, CacstudentOutcome studentOutcome)
        {
            if (id != studentOutcome.CacStudentOutcomeId)
            {
                return BadRequest();
            } else {
                _unitOfWork.CacstudentOutcomes.Update(studentOutcome);
            }
            return NoContent();
        }

        // DELETE api/<CacstudentOutcome>/SO1
        [HttpDelete("{id}")]
        public async Task<ActionResult<CacstudentOutcome>> Delete(string id)
        {
            _logger.LogInformation("Student Outcome Deleted");
            var studentOutcome = await _unitOfWork.CacstudentOutcomes.Get(id);

            if (studentOutcome == null) {
                return NotFound();
            }

            // Do deletion
            _unitOfWork.CacstudentOutcomes.Delete(studentOutcome);

            return studentOutcome;
        }
    }
}