using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using domain;
using domain.CourseInstanceAggregate;
using domain.AcademicYearAggregate;
using domain.SemesterAggregate;
using domain.CourseAggregate;
using domain.CourseLearningOutcomeAggregate;
using repository;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseInstanceController : ControllerBase
    {
        private readonly ILogger<CourseInstanceController> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public CourseInstanceController(ILogger<CourseInstanceController> logger,
                                      IUnitOfWork unitOfWork)

        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        // GET: api/<CourseInstance>
        [HttpGet]
        public async Task<IEnumerable<CourseInstance>> Get()
        {
            _logger.LogInformation("CourseInstance Called");
            return await _unitOfWork.CourseInstances.GetAll();
        }

        // GET api/<CourseInstance>/id
        [HttpGet("{id}")]
        public async Task<CourseInstance> Get(string id)
        {
            _logger.LogInformation("CourseInstance Called by ID");
            return await _unitOfWork.CourseInstances.Get(id);

        }

        // POST api/<CourseInstance>
        [HttpPost]
        public IActionResult Post()
        {
            var CourseInstance1 = new CourseInstance
            {
                CourseInstanceId = "331270",
                AcademicYear = new AcademicYear {
                    AcademicYearId = "2020-2021"
                },
                Course = new Course {
                    CourseId = "CIDM4390",
                    CourseName = "Software Systems Development"
                },
                Semester = new Semester {
                    SemesterId = "Spring"
                },
                CourseLearningOutcomes = new List<CourseLearningOutcome> {
                    new CourseLearningOutcome { CourseLearningOutcomeId = "CLO1", CourseLearningOutcomeDesc = "Analyze an organizational information system need to elicit system requirements"},
                    new CourseLearningOutcome { CourseLearningOutcomeId = "CLO2", CourseLearningOutcomeDesc = "Analyze an organizational information system need to elicit system requirements"}
                }
            };
            _unitOfWork.CourseInstances.Add(CourseInstance1);

            _logger.LogInformation("Course Instances Posted");
            _unitOfWork.Complete();
            return Ok();
        }

        // PUT api/<CourseInstance>/id
        [HttpPut("{id}")]
        public IActionResult Put(string id, CourseInstance courseInstance)
        {
            if (id != courseInstance.CourseInstanceId)
            {
                return BadRequest();
            } else {
                _unitOfWork.CourseInstances.Update(courseInstance);
            }
            return NoContent();
        }

        // DELETE api/<CourseInstance>/id
        [HttpDelete("{id}")]
        public async Task<ActionResult<CourseInstance>> Delete(string id)
        {
            _logger.LogInformation("Course Instance Deleted");
            var courseInstance = await _unitOfWork.CourseInstances.Get(id);

            if (courseInstance == null) {
                return NotFound();
            }

            // Do deletion
            _unitOfWork.CourseInstances.Delete(courseInstance);

            return courseInstance;
        }
    }
}