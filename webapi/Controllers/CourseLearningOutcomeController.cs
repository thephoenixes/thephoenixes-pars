using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using domain;
using domain.CourseInstanceAggregate;
using domain.AcademicYearAggregate;
using domain.SemesterAggregate;
using domain.CourseAggregate;
using domain.CourseLearningOutcomeAggregate;
using domain.CacstudentOutcomeAggregate;
using repository;

namespace webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseLearningOutcomeController : ControllerBase
    {
        private readonly ILogger<CourseLearningOutcomeController> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public CourseLearningOutcomeController(ILogger<CourseLearningOutcomeController> logger,
                                      IUnitOfWork unitOfWork)

        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        // GET: api/<CourseLearningOutcome>
        [HttpGet]
        public async Task<IEnumerable<CourseLearningOutcome>> Get()
        {
            _logger.LogInformation("CourseLearningOutcome Called");
            return await _unitOfWork.CourseLearningOutcomes.GetAll();
        }

        // GET api/<CourseLearningOutcome>/id
        [HttpGet("{id}")]
        public async Task<CourseLearningOutcome> Get(string id)
        {
            _logger.LogInformation("CourseLearningOutcome Called by ID");
            return await _unitOfWork.CourseLearningOutcomes.Get(id);

        }

        // POST api/<CourseLearningOutcome>
        [HttpPost]
        public IActionResult Post()
        {
            var CourseLearningOutcome1 = new CourseLearningOutcome
            {
                CourseLearningOutcomeId = "CLO1",
                CourseLearningOutcomeDesc = "Analyze an organizational information system need to elicit system requirements",
                CourseInstance = new CourseInstance {
                    CourseInstanceId = "439001",
                    AcademicYear = new AcademicYear {
                        AcademicYearId = "2020-2021"
                    },
                    Course = new Course {
                        CourseId = "CIDM4390",
                        CourseName = "Software Systems Development"
                    },
                    Semester = new Semester {
                        SemesterId = "Spring"
                    }
                },
                CacStudentOutcome = new CacstudentOutcome {
                    CacStudentOutcomeId = "SO1",
                    CacStudentOutcomeDesc = "An ability to analyze a complex computing problem and to apply principles of computing and other relevant disciplines to identify solutions."
                }
            };
            _unitOfWork.CourseLearningOutcomes.Add(CourseLearningOutcome1);

            _logger.LogInformation("CourseLearningOutcomes Posted");
            _unitOfWork.Complete();
            return Ok();
        }

        // PUT api/<CourseLearningOutcome>/id
        [HttpPut("{id}")]
        public IActionResult Put(string id, CourseLearningOutcome courseLearningOutcome)
        {
            if (id != courseLearningOutcome.CourseLearningOutcomeId)
            {
                return BadRequest();
            } else {
                _unitOfWork.CourseLearningOutcomes.Update(courseLearningOutcome);
            }
            return NoContent();
        }

        // DELETE api/<CourseLearningOutcome>/id
        [HttpDelete("{id}")]
        public async Task<ActionResult<CourseLearningOutcome>> Delete(string id)
        {
            _logger.LogInformation("Course Instance Deleted");
            var courseLearningOutcome = await _unitOfWork.CourseLearningOutcomes.Get(id);

            if (courseLearningOutcome == null) {
                return NotFound();
            }

            // Do deletion
            _unitOfWork.CourseLearningOutcomes.Delete(courseLearningOutcome);

            return courseLearningOutcome;
        }
    }
}