using System.Collections.Generic;
using System.Linq;
using domain.CourseAggregate;
using webapp.Data;

namespace repository
{
    public class CourseRepository : ICourseRepository
    {
        private ApplicationDbContext _context;

        public CourseRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool CreateCourse(Course course)
        {
            try
            {
                _context.Courses.Add(course);
                _context.SaveChanges();
                return true;
            } catch
            {
                return false;
            }
        }

        public List<Course> GetAllCourses()
        {
            return _context.Courses.ToList();
        }

        public Course GetCourseById(string courseId)
        {
            return _context.Courses.Where(x => x.CourseId == courseId).FirstOrDefault();
        }
    }
}