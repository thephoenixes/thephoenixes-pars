using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.SemesterAggregate;

namespace repository
{
    public class SemesterRepository : GenericRepository<Semester>, ISemesterRepository
    {
        public SemesterRepository(WebApiDbContext context) : base(context)
        {
            
        }

        // Place data retrieval methods here
        // Also declare the methods in domain/SemesterAggregate/ISemesterRepository.cs
        
        public Semester GetSemesterById(string id) {
            return _context.Semesters.Where(x => x.SemesterId == id).FirstOrDefault();
        } 
    }
}