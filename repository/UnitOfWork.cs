using System;
using domain;
using domain.AcademicYearAggregate;
using domain.AssessmentAggregate;
using domain.CacstudentOutcomeAggregate;
using domain.CourseAggregate;
using domain.CourseInstanceAggregate;
using domain.CourseInstanceCacstudentOutcomeAggregate;
using domain.CourseLearningOutcomeAggregate;
using domain.ProgramEducationalObjectiveAggregate;
using domain.SemesterAggregate;
using domain.FacultyQualitativeAssessmentReportAggregate;

namespace repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly WebApiDbContext _context;
        public IAcademicYearRepository AcademicYears { get; }
        public IAssessmentRepository Assessments { get; }
        public ICacstudentOutcomeRepository CacstudentOutcomes { get; }
        public ICourseRepository Courses { get; }
        public ICourseInstanceRepository CourseInstances { get; }
        public ICourseInstanceCacstudentOutcomeRepository CourseInstanceCacstudentOutcomes { get; }
        public ICourseLearningOutcomeRepository CourseLearningOutcomes { get; }
        public IProgramEducationalObjectiveRepository ProgramEducationalObjectives { get; }
        public ISemesterRepository Semesters { get; }
        public IFacultyQualitativeAssessmentReportRepository FacultyQualitativeAssessmentReports { get; set; }

        public UnitOfWork(WebApiDbContext webapiDbContext, 
                          IAcademicYearRepository academicYearsRepository, 
                          IAssessmentRepository assessmentsRepository,
                          ICacstudentOutcomeRepository cacstudentOutcomesRepository,
                          ICourseRepository coursesRepository,
                          ICourseInstanceRepository courseInstancesRepository,
                          ICourseInstanceCacstudentOutcomeRepository courseInstanceCacstudentOutcomesRepository,
                          ICourseLearningOutcomeRepository courseLearningOutcomesRepository,
                          IProgramEducationalObjectiveRepository programEducationalObjectivesRepository,
                          ISemesterRepository semesterRepository,
                          IFacultyQualitativeAssessmentReportRepository facultyQualitativeAssessmentReportRepository)
        {
            this._context = webapiDbContext;
            this.AcademicYears = academicYearsRepository;
            this.Assessments = assessmentsRepository;
            this.CacstudentOutcomes = cacstudentOutcomesRepository;
            this.Courses = coursesRepository;
            this.CourseInstances = courseInstancesRepository;
            this.CourseInstanceCacstudentOutcomes = courseInstanceCacstudentOutcomesRepository;
            this.CourseLearningOutcomes = courseLearningOutcomesRepository;
            this.ProgramEducationalObjectives = programEducationalObjectivesRepository;
            this.Semesters = semesterRepository;
            this.FacultyQualitativeAssessmentReports = facultyQualitativeAssessmentReportRepository;
        }
        
        public int Complete()
        {
            return _context.SaveChanges();
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}