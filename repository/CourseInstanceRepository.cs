using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.CourseInstanceAggregate;

namespace repository
{
    public class CourseInstanceRepository : GenericRepository<CourseInstance>, ICourseInstanceRepository
    {
        public CourseInstanceRepository(WebApiDbContext context) : base(context)
        {
            
        }

        // Place data retrieval methods here
        // Also declare the methods in domain/CourseInstanceAggregate/ICourseInstanceRepository.cs
        
        // public IEnumerable<NOAAStation> GetStationsByState(string StateAbbreviation) {
        //     return _context.Stations.Where(x => x.StateAbbreviation == StateAbbreviation);
        // } 
    }
}