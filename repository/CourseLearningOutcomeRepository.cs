using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.CourseLearningOutcomeAggregate;

namespace repository
{
    public class CourseLearningOutcomeRepository : GenericRepository<CourseLearningOutcome>, ICourseLearningOutcomeRepository
    {
        public CourseLearningOutcomeRepository(WebApiDbContext context) : base(context)
        {
            
        }

        // Place data retrieval methods here
        // Also declare the methods in domain/CourseLearningOutcomeAggregate/ICourseLearningOutcomeRepository.cs
        
        // public IEnumerable<NOAAStation> GetStationsByState(string StateAbbreviation) {
        //     return _context.Stations.Where(x => x.StateAbbreviation == StateAbbreviation);
        // } 
    }
}