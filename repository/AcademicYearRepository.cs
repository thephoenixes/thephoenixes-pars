using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.AcademicYearAggregate;

namespace repository
{
    public class AcademicYearRepository : GenericRepository<AcademicYear>, IAcademicYearRepository
    {
        public AcademicYearRepository(WebApiDbContext context) : base(context)
        {
            
        }

        // Place data retrieval methods here
        // Also declare the methods in domain/AcademicYearAggregate/IAcademicYearRepository.cs
        
        // public IEnumerable<NOAAStation> GetStationsByState(string StateAbbreviation) {
        //     return _context.Stations.Where(x => x.StateAbbreviation == StateAbbreviation);
        // } 
    }
}