using domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using domain.AcademicYearAggregate;
using domain.AssessmentAggregate;
using domain.CacstudentOutcomeAggregate;
using domain.CourseAggregate;
using domain.CourseInstanceAggregate;
using domain.CourseInstanceCacstudentOutcomeAggregate;
using domain.CourseLearningOutcomeAggregate;
using domain.ProgramEducationalObjectiveAggregate;
using domain.SemesterAggregate;
using domain.FacultyQualitativeAssessmentReportAggregate;


namespace repository
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {

            services.AddTransient<IAcademicYearRepository, AcademicYearRepository>();
            services.AddTransient<IAssessmentRepository, AssessmentRepository>();
            services.AddTransient<ICacstudentOutcomeRepository, CacstudentOutcomeRepository>();
            services.AddTransient<ICourseRepository, CourseRepository>();
            services.AddTransient<ICourseInstanceRepository, CourseInstanceRepository>();
            services.AddTransient<ICourseInstanceCacstudentOutcomeRepository, CourseInstanceCacstudentOutcomeRepository>();
            services.AddTransient<ICourseLearningOutcomeRepository, CourseLearningOutcomeRepository>();
            services.AddTransient<IProgramEducationalObjectiveRepository, ProgramEducationalObjectiveRepository>();
            services.AddTransient<ISemesterRepository, SemesterRepository>();
            services.AddTransient<IFacultyQualitativeAssessmentReportRepository, FacultyQualitativeAssessmentReportRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddDbContext<WebApiDbContext>(options => options.UseInMemoryDatabase(databaseName: "PARS"));            
            
            return services;
        }
    }
}