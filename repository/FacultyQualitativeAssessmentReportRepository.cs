using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.FacultyQualitativeAssessmentReportAggregate;

namespace repository
{
    public class FacultyQualitativeAssessmentReportRepository : GenericRepository<FacultyQualitativeAssessmentReport>, IFacultyQualitativeAssessmentReportRepository
    {
        public FacultyQualitativeAssessmentReportRepository(WebApiDbContext context) : base(context)
        {
            
        }

        // Place data retrieval methods here
        // Also declare the methods in domain/FacultyQualitativeAssessmentReportAggregate/IFacultyQualitativeAssessmentReportRepository.cs
        
        // public IEnumerable<NOAAStation> GetStationsByState(string StateAbbreviation) {
        //     return _context.Stations.Where(x => x.StateAbbreviation == StateAbbreviation);
        // } 
    }
}