using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.CourseInstanceCacstudentOutcomeAggregate;

namespace repository
{
    public class CourseInstanceCacstudentOutcomeRepository : GenericRepository<CourseInstanceCacstudentOutcome>, ICourseInstanceCacstudentOutcomeRepository
    {
        public CourseInstanceCacstudentOutcomeRepository(WebApiDbContext context) : base(context)
        {
            
        }

        // Place data retrieval methods here
        // Also declare the methods in domain/CourseInstanceCacstudentOutcomeAggregate/ICourseInstanceCacstudentOutcomeRepository.cs
        
        // public IEnumerable<NOAAStation> GetStationsByState(string StateAbbreviation) {
        //     return _context.Stations.Where(x => x.StateAbbreviation == StateAbbreviation);
        // } 
    }
}