using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.CacstudentOutcomeAggregate;

namespace repository
{
    public class CacstudentOutcomeRepository : GenericRepository<CacstudentOutcome>, ICacstudentOutcomeRepository
    {
        public CacstudentOutcomeRepository(WebApiDbContext context) : base(context)
        {
        }

        // Place data retrieval methods here
        // Also declare the methods in domain/CacstudentOutcomeAggregate/ICacstudentOutcomeRepository.cs
        
        // public IEnumerable<CacstudentOutcome> GetStudentOutcomesById(string id)
        // {
        //     return _context.CacstudentOutcome.Where(x => x.CacStudentOutcomeId == id);
        // }
    }
}