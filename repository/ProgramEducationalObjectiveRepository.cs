using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.ProgramEducationalObjectiveAggregate;

namespace repository
{
    public class ProgramEducationalObjectiveRepository : GenericRepository<ProgramEducationalObjective>, IProgramEducationalObjectiveRepository
    {
        public ProgramEducationalObjectiveRepository(WebApiDbContext context) : base(context)
        {
            
        }

        // Place data retrieval methods here
        // Also declare the methods in domain/ProgramEducationalObjectiveAggregate/IProgramEducationalObjectiveRepository.cs
        
        // public IEnumerable<NOAAStation> GetStationsByState(string StateAbbreviation) {
        //     return _context.Stations.Where(x => x.StateAbbreviation == StateAbbreviation);
        // } 
    }
}