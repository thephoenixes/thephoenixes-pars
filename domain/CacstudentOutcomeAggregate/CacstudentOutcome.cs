﻿using System;
using System.Collections.Generic;
using domain.CourseInstanceCacstudentOutcomeAggregate;
using domain.CourseLearningOutcomeAggregate;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace domain.CacstudentOutcomeAggregate
{
    public partial class CacstudentOutcome
    {
        public CacstudentOutcome()
        {
            CourseInstanceCacstudentOutcomes = new HashSet<CourseInstanceCacstudentOutcome>();
            CourseLearningOutcomes = new HashSet<CourseLearningOutcome>();
        }

        [Display(Name = "Student Outcome ID")]
        public string CacStudentOutcomeId { get; set; }

        [Display(Name = "Student Outcome Description")]
        public string CacStudentOutcomeDesc { get; set; }

        public virtual ICollection<CourseInstanceCacstudentOutcome> CourseInstanceCacstudentOutcomes { get; set; }
        public virtual ICollection<CourseLearningOutcome> CourseLearningOutcomes { get; set; }
    }
}
