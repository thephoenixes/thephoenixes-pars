using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace domain.CacstudentOutcomeAggregate
{
    public interface ICacstudentOutcomeRepository : IGenericRepository<CacstudentOutcome>
    {
        // IEnumerable<CacstudentOutcome> GetStudentOutcomeById(string id);
    }
}