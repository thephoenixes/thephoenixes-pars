using System;
using domain.AcademicYearAggregate;
using domain.AssessmentAggregate;
using domain.CacstudentOutcomeAggregate;
using domain.CourseAggregate;
using domain.CourseInstanceAggregate;
using domain.CourseInstanceCacstudentOutcomeAggregate;
using domain.CourseLearningOutcomeAggregate;
using domain.ProgramEducationalObjectiveAggregate;
using domain.SemesterAggregate;
using domain.FacultyQualitativeAssessmentReportAggregate;

namespace domain
{
    public interface IUnitOfWork : IDisposable
    {
        IAcademicYearRepository AcademicYears { get; }
        IAssessmentRepository Assessments { get; }
        ICacstudentOutcomeRepository CacstudentOutcomes { get; }
        ICourseRepository Courses { get; }
        ICourseInstanceRepository CourseInstances { get; }
        ICourseInstanceCacstudentOutcomeRepository CourseInstanceCacstudentOutcomes { get; }
        ICourseLearningOutcomeRepository CourseLearningOutcomes { get; }
        IProgramEducationalObjectiveRepository ProgramEducationalObjectives { get; }
        ISemesterRepository Semesters { get; }
        IFacultyQualitativeAssessmentReportRepository FacultyQualitativeAssessmentReports { get; }
        int Complete();
    }
}