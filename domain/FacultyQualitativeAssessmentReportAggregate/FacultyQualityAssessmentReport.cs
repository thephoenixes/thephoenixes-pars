using domain.CourseInstanceAggregate;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace domain.FacultyQualitativeAssessmentReportAggregate
{
    public partial class FacultyQualitativeAssessmentReport
    {
        public int FacultyQualitativeAssessmentReportId { get; set; }

        // What did you put in new last time?
        [Display(Name = "What did you put in new last time?")]
        public string New { get; set; }

        // What were your observations this time/how'd it go?
        [Display(Name = "What were your observations this time/how'd it go?")]
        public string Observations { get; set; }

        // Did the students struggle more over something specific? Did they like this/that?
        [Display(Name = "Did the students struggle more over something specific? Did they like this/that?")]
        public string StudentFeedback { get; set; }

        // What do I want to change next time?
        [Display(Name = "What do I want to change next time?")]
        public string Change { get; set; }

        public string CourseInstanceId { get; set; }

        [Display(Name = "Course Instance")]
        public virtual CourseInstance CourseInstance { get; set; }
    }
}
