namespace domain.FacultyQualitativeAssessmentReportAggregate
{
    public interface IFacultyQualitativeAssessmentReportRepository : IGenericRepository<FacultyQualitativeAssessmentReport>
    {
    }
}