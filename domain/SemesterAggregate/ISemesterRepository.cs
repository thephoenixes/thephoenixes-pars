using System.Collections.Generic;

namespace domain.SemesterAggregate
{
    public interface ISemesterRepository : IGenericRepository<Semester>
    {
        Semester GetSemesterById(string id);
    }
}