﻿using System;
using System.Collections.Generic;
using domain.CourseInstanceAggregate;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace domain.SemesterAggregate
{
    public partial class Semester
    {
        public Semester()
        {
            CourseInstances = new HashSet<CourseInstance>();
        }

        public string SemesterId { get; set; }

        public virtual ICollection<CourseInstance> CourseInstances { get; set; }
    }
}
