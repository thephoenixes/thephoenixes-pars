namespace domain.CourseInstanceCacstudentOutcomeAggregate
{
    public interface ICourseInstanceCacstudentOutcomeRepository : IGenericRepository<CourseInstanceCacstudentOutcome>
    {
    }
}