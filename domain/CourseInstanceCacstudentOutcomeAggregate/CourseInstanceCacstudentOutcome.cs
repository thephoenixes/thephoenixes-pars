﻿using System;
using System.Collections.Generic;
using domain.CacstudentOutcomeAggregate;
using domain.CourseInstanceAggregate;

#nullable disable

namespace domain.CourseInstanceCacstudentOutcomeAggregate
{
    public partial class CourseInstanceCacstudentOutcome
    {
        public string CourseInstanceId { get; set; }
        public string CacStudentOutcomeId { get; set; }

        public virtual CacstudentOutcome CacStudentOutcome { get; set; }
        public virtual CourseInstance CourseInstance { get; set; }
    }
}
