﻿using System;
using System.Collections.Generic;
using domain.AssessmentAggregate;
using domain.CacstudentOutcomeAggregate;
using domain.CourseInstanceAggregate;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace domain.CourseLearningOutcomeAggregate
{
    public partial class CourseLearningOutcome
    {
        public CourseLearningOutcome()
        {
            Assessments = new HashSet<Assessment>();
        }

        [Display(Name = "CLO ID")]
        public string CourseLearningOutcomeId { get; set; }

        [Display(Name = "CLO Description")]
        public string CourseLearningOutcomeDesc { get; set; }

        [Display(Name = "Course Instance ID")]
        public string CourseInstanceId { get; set; }

        [Display(Name = "Assessment ID")]
        public long? AssessmentId { get; set; }

        [Display(Name = "Student Outcome ID")]
        public string CacStudentOutcomeId { get; set; }

        public virtual ICollection<Assessment> Assessments { get; set; }

        [Display(Name = "Student Outcome")]
        public virtual CacstudentOutcome CacStudentOutcome { get; set; }

        [Display(Name = "Course Instance")]
        public virtual CourseInstance CourseInstance { get; set; }
    }
}
