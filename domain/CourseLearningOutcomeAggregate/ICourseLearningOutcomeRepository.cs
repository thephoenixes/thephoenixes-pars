namespace domain.CourseLearningOutcomeAggregate
{
    public interface ICourseLearningOutcomeRepository : IGenericRepository<CourseLearningOutcome>
    {
    }
}