namespace domain.CourseInstanceAggregate
{
    public interface ICourseInstanceRepository : IGenericRepository<CourseInstance>
    {
    }
}