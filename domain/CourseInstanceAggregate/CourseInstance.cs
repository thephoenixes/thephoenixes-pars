﻿using System;
using System.Collections.Generic;
using domain.AcademicYearAggregate;
using domain.CourseAggregate;
using domain.SemesterAggregate;
using domain.CourseInstanceCacstudentOutcomeAggregate;
using domain.CourseLearningOutcomeAggregate;
using domain.FacultyQualitativeAssessmentReportAggregate;
using System.ComponentModel.DataAnnotations;


#nullable disable

namespace domain.CourseInstanceAggregate
{
    public partial class CourseInstance
    {
        public CourseInstance()
        {
            CourseInstanceCacstudentOutcomes = new HashSet<CourseInstanceCacstudentOutcome>();
            CourseLearningOutcomes = new HashSet<CourseLearningOutcome>();
        }

        [Display(Name = "Course Instance ID")]
        public string CourseInstanceId { get; set; }

        [Display(Name = "Academic Year")]
        public string AcademicYearId { get; set; }

        [Display(Name = "Semester")]
        public string SemesterId { get; set; }

        [Display(Name = "Course")]
        public string CourseId { get; set; }

        [Display(Name = "Faculty Qualitative Assessment Report ID")]
        public int? FacultyQualitativeAssessmentReportId { get; set; }

        [Display(Name = "Academic Year")]
        public virtual AcademicYear AcademicYear { get; set; }
        public virtual Course Course { get; set; }
        public virtual Semester Semester { get; set; }
        public virtual FacultyQualitativeAssessmentReport FacultyQualitativeAssessmentReport { get; set; }
        public virtual ICollection<CourseInstanceCacstudentOutcome> CourseInstanceCacstudentOutcomes { get; set; }
        public virtual ICollection<CourseLearningOutcome> CourseLearningOutcomes { get; set; }
    }
}
