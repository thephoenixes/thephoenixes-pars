﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace domain.ProgramEducationalObjectiveAggregate
{
    public partial class ProgramEducationalObjective
    {

        [Display(Name = "PEO ID")]
        public string ProgramEducationalObjectiveId { get; set; }

        [Display(Name = "PEO Description")]
        public string ProgramEducationalObjectiveDesc { get; set; }
    }
}
