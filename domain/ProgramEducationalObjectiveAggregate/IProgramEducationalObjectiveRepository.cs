namespace domain.ProgramEducationalObjectiveAggregate
{
    public interface IProgramEducationalObjectiveRepository : IGenericRepository<ProgramEducationalObjective>
    {
    }
}