﻿using System;
using System.Collections.Generic;
using domain.CourseInstanceAggregate;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace domain.CourseAggregate
{
    public partial class Course
    {
        public Course()
        {
            CourseInstances = new HashSet<CourseInstance>();
        }

        [Display(Name = "Course ID")]
        public string CourseId { get; set; }

        [Display(Name = "Course Name")]
        public string CourseName { get; set; }

        public virtual ICollection<CourseInstance> CourseInstances { get; set; }
    }
}
