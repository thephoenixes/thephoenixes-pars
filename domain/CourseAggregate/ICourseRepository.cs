using System.Collections.Generic;

namespace domain.CourseAggregate
{
    public interface ICourseRepository
    {

        bool CreateCourse(Course course);

        List<Course> GetAllCourses();

        Course GetCourseById(string id);
    }
}