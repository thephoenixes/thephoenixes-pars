﻿using System;
using System.Collections.Generic;
using domain.CourseInstanceAggregate;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace domain.AcademicYearAggregate
{
    public partial class AcademicYear
    {
        public AcademicYear()
        {
            CourseInstances = new HashSet<CourseInstance>();
        }

        [Display(Name = "Academic Year")]
        public string AcademicYearId { get; set; }

        public virtual ICollection<CourseInstance> CourseInstances { get; set; }
    }
}
