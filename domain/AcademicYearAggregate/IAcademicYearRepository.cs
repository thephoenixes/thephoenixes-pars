namespace domain.AcademicYearAggregate
{
    public interface IAcademicYearRepository : IGenericRepository<AcademicYear>
    {
    }
}