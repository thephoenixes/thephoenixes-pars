namespace domain.AssessmentAggregate
{
    public interface IAssessmentRepository : IGenericRepository<Assessment>
    {
    }
}