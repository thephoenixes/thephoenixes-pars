﻿using System;
using System.Collections.Generic;
using domain.CourseLearningOutcomeAggregate;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace domain.AssessmentAggregate
{
    public partial class Assessment
    {

        [Display(Name = "Assessment ID")]
        public long AssessmentId { get; set; }

        [Display(Name = "Instrument")]
        public string AssessmentInstrument { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be an integer greater than or equal to 0")]
        [Display(Name = "Excellent")]
        public int ExcellentStudents { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be an integer greater than or equal to 0")]
        [Display(Name = "Good")]
        public int GoodStudents { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be an integer greater than or equal to 0")]
        [Display(Name = "Satisfactory")]
        public int SatisfactoryStudents { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be an integer greater than or equal to 0")]
        [Display(Name = "Unsatisfactory")]
        public int UnsatisfactoryStudents { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be an integer greater than or equal to 0")]
        [Display(Name = "Missing")]
        public int MissingStudents { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Value should be an integer greater than or equal to 0")]
        [SumMatchesTotal("ExcellentStudents, GoodStudents, SatisfactoryStudents, UnsatisfactoryStudents, MissingStudents", ErrorMessage = "Total of students doesn't match the sum of the students.")]
        [Display(Name = "Total")]
        public int TotalStudents { get; set; }

        [Display(Name = "CLO ID")]
        public string CourseLearningOutcomeId { get; set; }

        [Display(Name = "CLO ID")]
        public virtual CourseLearningOutcome CourseLearningOutcome { get; set; }
    }

    public class SumMatchesTotalAttribute : ValidationAttribute
    {
        private readonly string _comparisonProperty;

        public SumMatchesTotalAttribute(string comparisonProperty)
        {
            _comparisonProperty = comparisonProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ErrorMessage = ErrorMessageString;
            var currentValue = (int)value;

            var allValues = _comparisonProperty.Split(", ");

            var comparisonValue = 0;

            foreach (var parameterToSum in allValues)
            {
                var property = validationContext.ObjectType.GetProperty(parameterToSum);

                comparisonValue += (int)property.GetValue(validationContext.ObjectInstance);
            }

            if (currentValue != comparisonValue)
                return new ValidationResult(ErrorMessage);

            return ValidationResult.Success;
        }
    }
}
