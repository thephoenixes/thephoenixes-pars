﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace webapp.Migrations
{
    public partial class Reinitialized : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AcademicYear",
                columns: table => new
                {
                    academicYearID = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AcademicYear", x => x.academicYearID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    UserName = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "INTEGER", nullable: false),
                    PasswordHash = table.Column<string>(type: "TEXT", nullable: true),
                    SecurityStamp = table.Column<string>(type: "TEXT", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "TEXT", nullable: true),
                    PhoneNumber = table.Column<string>(type: "TEXT", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "INTEGER", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "TEXT", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CACStudentOutcome",
                columns: table => new
                {
                    cacStudentOutcomeID = table.Column<string>(type: "TEXT", nullable: false),
                    cacStudentOutcomeDesc = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CACStudentOutcome", x => x.cacStudentOutcomeID);
                });

            migrationBuilder.CreateTable(
                name: "Course",
                columns: table => new
                {
                    CourseID = table.Column<string>(type: "TEXT", nullable: false),
                    courseName = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Course", x => x.CourseID);
                });

            migrationBuilder.CreateTable(
                name: "ProgramEducationalObjective",
                columns: table => new
                {
                    programEducationalObjectiveID = table.Column<string>(type: "TEXT", nullable: false),
                    programEducationalObjectiveDesc = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramEducationalObjective", x => x.programEducationalObjectiveID);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "TEXT", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Semester",
                columns: table => new
                {
                    semesterID = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Semester", x => x.semesterID);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    FirstName = table.Column<string>(type: "TEXT", nullable: true),
                    LastName = table.Column<string>(type: "TEXT", nullable: true),
                    UsernameChangeLimit = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_AspNetUsers_Id",
                        column: x => x.Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    ClaimType = table.Column<string>(type: "TEXT", nullable: true),
                    ClaimValue = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "TEXT", nullable: false),
                    ProviderKey = table.Column<string>(type: "TEXT", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "TEXT", nullable: true),
                    UserId = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    LoginProvider = table.Column<string>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Value = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_UserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    RoleId = table.Column<string>(type: "TEXT", nullable: false),
                    ClaimType = table.Column<string>(type: "TEXT", nullable: true),
                    ClaimValue = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RoleClaims_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "TEXT", nullable: false),
                    RoleId = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoles_Role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CourseInstance",
                columns: table => new
                {
                    courseInstanceID = table.Column<string>(type: "TEXT", nullable: false),
                    academicYearID = table.Column<string>(type: "TEXT", nullable: true),
                    semesterID = table.Column<string>(type: "TEXT", nullable: true),
                    courseID = table.Column<string>(type: "TEXT", nullable: true),
                    facultyQualitativeAssessmentReportID = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseInstance", x => x.courseInstanceID);
                    table.ForeignKey(
                        name: "FK_CourseInstance_AcademicYear_academicYearID",
                        column: x => x.academicYearID,
                        principalTable: "AcademicYear",
                        principalColumn: "academicYearID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CourseInstance_Course_courseID",
                        column: x => x.courseID,
                        principalTable: "Course",
                        principalColumn: "CourseID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CourseInstance_Semester_semesterID",
                        column: x => x.semesterID,
                        principalTable: "Semester",
                        principalColumn: "semesterID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CourseInstance-CACStudentOutcome",
                columns: table => new
                {
                    courseInstanceID = table.Column<string>(type: "TEXT", nullable: false),
                    cacStudentOutcomeID = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseInstance-CACStudentOutcome", x => new { x.courseInstanceID, x.cacStudentOutcomeID });
                    table.ForeignKey(
                        name: "FK_CourseInstance-CACStudentOutcome_CACStudentOutcome_cacStudentOutcomeID",
                        column: x => x.cacStudentOutcomeID,
                        principalTable: "CACStudentOutcome",
                        principalColumn: "cacStudentOutcomeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CourseInstance-CACStudentOutcome_CourseInstance_courseInstanceID",
                        column: x => x.courseInstanceID,
                        principalTable: "CourseInstance",
                        principalColumn: "courseInstanceID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CourseLearningOutcome",
                columns: table => new
                {
                    courseLearningOutcomeID = table.Column<string>(type: "TEXT", nullable: false),
                    courseLearningOutcomeDesc = table.Column<string>(type: "TEXT", nullable: true),
                    courseInstanceID = table.Column<string>(type: "TEXT", nullable: true),
                    assessmentID = table.Column<long>(type: "INTEGER", nullable: true),
                    cacStudentOutcomeID = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseLearningOutcome", x => x.courseLearningOutcomeID);
                    table.ForeignKey(
                        name: "FK_CourseLearningOutcome_CACStudentOutcome_cacStudentOutcomeID",
                        column: x => x.cacStudentOutcomeID,
                        principalTable: "CACStudentOutcome",
                        principalColumn: "cacStudentOutcomeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CourseLearningOutcome_CourseInstance_courseInstanceID",
                        column: x => x.courseInstanceID,
                        principalTable: "CourseInstance",
                        principalColumn: "courseInstanceID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FacultyQualitativeAssessmentReport",
                columns: table => new
                {
                    facultyQualitativeAssessmentReportID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    @new = table.Column<string>(name: "new", type: "TEXT", nullable: true),
                    observations = table.Column<string>(type: "TEXT", nullable: true),
                    studentFeedback = table.Column<string>(type: "TEXT", nullable: true),
                    change = table.Column<string>(type: "TEXT", nullable: true),
                    courseInstanceID = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FacultyQualitativeAssessmentReport", x => x.facultyQualitativeAssessmentReportID);
                    table.ForeignKey(
                        name: "FK_FacultyQualitativeAssessmentReport_CourseInstance_courseInstanceID",
                        column: x => x.courseInstanceID,
                        principalTable: "CourseInstance",
                        principalColumn: "courseInstanceID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Assessment",
                columns: table => new
                {
                    assessmentID = table.Column<long>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    assessmentInstrument = table.Column<string>(type: "TEXT", nullable: true),
                    excellentStudents = table.Column<int>(type: "INTEGER", nullable: false),
                    goodStudents = table.Column<int>(type: "INTEGER", nullable: false),
                    satisfactoryStudents = table.Column<int>(type: "INTEGER", nullable: false),
                    unsatisfactoryStudents = table.Column<int>(type: "INTEGER", nullable: false),
                    missingStudents = table.Column<int>(type: "INTEGER", nullable: false),
                    totalStudents = table.Column<int>(type: "INTEGER", nullable: false),
                    courseLearningOutcomeID = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assessment", x => x.assessmentID);
                    table.ForeignKey(
                        name: "FK_Assessment_CourseLearningOutcome_courseLearningOutcomeID",
                        column: x => x.courseLearningOutcomeID,
                        principalTable: "CourseLearningOutcome",
                        principalColumn: "courseLearningOutcomeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Assessment_assessmentID",
                table: "Assessment",
                column: "assessmentID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Assessment_courseLearningOutcomeID",
                table: "Assessment",
                column: "courseLearningOutcomeID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseInstance_academicYearID",
                table: "CourseInstance",
                column: "academicYearID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseInstance_courseID",
                table: "CourseInstance",
                column: "courseID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseInstance_courseInstanceID",
                table: "CourseInstance",
                column: "courseInstanceID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CourseInstance_semesterID",
                table: "CourseInstance",
                column: "semesterID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseInstance-CACStudentOutcome_cacStudentOutcomeID",
                table: "CourseInstance-CACStudentOutcome",
                column: "cacStudentOutcomeID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseInstance-CACStudentOutcome_courseInstanceID",
                table: "CourseInstance-CACStudentOutcome",
                column: "courseInstanceID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseLearningOutcome_cacStudentOutcomeID",
                table: "CourseLearningOutcome",
                column: "cacStudentOutcomeID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseLearningOutcome_courseInstanceID",
                table: "CourseLearningOutcome",
                column: "courseInstanceID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseLearningOutcome_courseLearningOutcomeID",
                table: "CourseLearningOutcome",
                column: "courseLearningOutcomeID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FacultyQualitativeAssessmentReport_courseInstanceID",
                table: "FacultyQualitativeAssessmentReport",
                column: "courseInstanceID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FacultyQualitativeAssessmentReport_FacultyQualitativeAssessmentReportID",
                table: "FacultyQualitativeAssessmentReport",
                column: "facultyQualitativeAssessmentReportID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Role",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RoleClaims_RoleId",
                table: "RoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserClaims_UserId",
                table: "UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserId",
                table: "UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Assessment");

            migrationBuilder.DropTable(
                name: "CourseInstance-CACStudentOutcome");

            migrationBuilder.DropTable(
                name: "FacultyQualitativeAssessmentReport");

            migrationBuilder.DropTable(
                name: "ProgramEducationalObjective");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserTokens");

            migrationBuilder.DropTable(
                name: "CourseLearningOutcome");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "CACStudentOutcome");

            migrationBuilder.DropTable(
                name: "CourseInstance");

            migrationBuilder.DropTable(
                name: "AcademicYear");

            migrationBuilder.DropTable(
                name: "Course");

            migrationBuilder.DropTable(
                name: "Semester");
        }
    }
}
