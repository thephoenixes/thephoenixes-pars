using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;
using domain;
using domain.AcademicYearAggregate;
using domain.AssessmentAggregate;
using domain.CacstudentOutcomeAggregate;
using domain.CourseAggregate;
using domain.CourseInstanceAggregate;
using domain.CourseInstanceCacstudentOutcomeAggregate;
using domain.CourseLearningOutcomeAggregate;
using domain.ProgramEducationalObjectiveAggregate;
using domain.SemesterAggregate;
using domain.FacultyQualitativeAssessmentReportAggregate;

namespace webapp.Data
{
    public static class ContextSeed
    {
        public static async Task SeedRolesAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Seed Roles
            await roleManager.CreateAsync(new IdentityRole(Enums.Roles.Admin.ToString()));
            await roleManager.CreateAsync(new IdentityRole(Enums.Roles.Professor.ToString()));
            await roleManager.CreateAsync(new IdentityRole(Enums.Roles.Evaluator.ToString()));
        }
        public static async Task SeedSuperAdminAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Seed Default User
            var defaultUser = new ApplicationUser
            {
                UserName = "admin",
                Email = "thephoenixesteam@gmail.com",
                FirstName = "Admin",
                LastName = "User",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };
            if (userManager.Users.All(u => u.Id != defaultUser.Id))
            {
                var user = await userManager.FindByEmailAsync(defaultUser.Email);
                if (user == null)
                {
                    await userManager.CreateAsync(defaultUser, "123Pa$$word");
                    await userManager.AddToRoleAsync(defaultUser, Enums.Roles.Admin.ToString());
                }

            }
        }

        public static async Task SeedProfessorAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Seed Default User
            var defaultUser = new ApplicationUser
            {
                UserName = "professor",
                Email = "thephoenixesteam+professor@gmail.com",
                FirstName = "Professor",
                LastName = "User",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };
            if (userManager.Users.All(u => u.Id != defaultUser.Id))
            {
                var user = await userManager.FindByEmailAsync(defaultUser.Email);
                if (user == null)
                {
                    await userManager.CreateAsync(defaultUser, "123Pa$$word");
                    await userManager.AddToRoleAsync(defaultUser, Enums.Roles.Professor.ToString());
                }

            }
        }

        public static async Task SeedEvaluatorAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            //Seed Default User
            var defaultUser = new ApplicationUser
            {
                UserName = "evaluator",
                Email = "thephoenixesteam+evaluator@gmail.com",
                FirstName = "Evaluator",
                LastName = "User",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };
            if (userManager.Users.All(u => u.Id != defaultUser.Id))
            {
                var user = await userManager.FindByEmailAsync(defaultUser.Email);
                if (user == null)
                {
                    await userManager.CreateAsync(defaultUser, "123Pa$$word");
                    await userManager.AddToRoleAsync(defaultUser, Enums.Roles.Evaluator.ToString());
                }

            }
        }

        public static void SeedProgramEducationalObjective(ApplicationDbContext context)
        {
            //Adding ProgramEducationalObjective to the database
            if (!context.ProgramEducationalObjectives.Any(x => x.ProgramEducationalObjectiveId == "PEO1"))
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO1", ProgramEducationalObjectiveDesc = "(Applied Skills and Knowledge) Apply foundational knowledge and technical skills to work in a business environment." });
            if (!context.ProgramEducationalObjectives.Any(x => x.ProgramEducationalObjectiveId == "PEO2"))
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO2", ProgramEducationalObjectiveDesc = "(Analysis and Design) Synthesize core knowledge of computer information systems to analyze, design, and implement solutions to real-wordl business problems." });
            if (!context.ProgramEducationalObjectives.Any(x => x.ProgramEducationalObjectiveId == "PEO3"))
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO3", ProgramEducationalObjectiveDesc = "(Currency) Actively engage in life-long learning and professional development." });
            if (!context.ProgramEducationalObjectives.Any(x => x.ProgramEducationalObjectiveId == "PEO4"))
                context.ProgramEducationalObjectives.Add(new ProgramEducationalObjective { ProgramEducationalObjectiveId = "PEO4", ProgramEducationalObjectiveDesc = "(Teamwork) Work in teams to solve business problems by utilizing effective communication and collaboration skills." });

            context.SaveChanges();
        }

        public static void SeedStudentOutcome(ApplicationDbContext context)
        {
            //Adding ProgramEducationalObjective to the database
            if (!context.CacstudentOutcomes.Any(x => x.CacStudentOutcomeId == "SO1"))
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO1", CacStudentOutcomeDesc = "An ability to analyze a complex computing problem and to apply principles of computing and other releant disciplines to identify solutions." });
            if (!context.CacstudentOutcomes.Any(x => x.CacStudentOutcomeId == "SO2"))
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO2", CacStudentOutcomeDesc = "An ability to design, implement, and evaluate a computing-based solution to meet a given set of computing requirements in the context of the program's discipline." });
            if (!context.CacstudentOutcomes.Any(x => x.CacStudentOutcomeId == "SO3"))
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO3", CacStudentOutcomeDesc = "An ability to communicate effectively in a variety of professional contexts." });
            if (!context.CacstudentOutcomes.Any(x => x.CacStudentOutcomeId == "SO4"))
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO4", CacStudentOutcomeDesc = "An ability to recognize professional responsibilities and make informed judgments in computing practice based on legal and ethical principles." });
            if (!context.CacstudentOutcomes.Any(x => x.CacStudentOutcomeId == "SO5"))
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO5", CacStudentOutcomeDesc = "An ability to function effectively as a member or leader of a team engaged in activities appropriate to the program's discipline." });
            if (!context.CacstudentOutcomes.Any(x => x.CacStudentOutcomeId == "SO6"))
                context.CacstudentOutcomes.Add(new CacstudentOutcome { CacStudentOutcomeId = "SO6", CacStudentOutcomeDesc = "An ability to support the delivery, use, and management of information systems within an information systems environment." });

            context.SaveChanges();
        }

        public static void SeedAcademicYear(ApplicationDbContext context)
        {
            if (!context.AcademicYears.Any(x => x.AcademicYearId == "2019-2020"))
                context.AcademicYears.Add(new AcademicYear { AcademicYearId = "2019-2020"});
            if (!context.AcademicYears.Any(x => x.AcademicYearId == "2020-2021"))
                context.AcademicYears.Add(new AcademicYear { AcademicYearId = "2020-2021"});
            context.SaveChanges();
        }

        public static void SeedSemester(ApplicationDbContext context)
        {
            if (!context.Semesters.Any(x => x.SemesterId == "Fall"))
                context.Semesters.Add(new Semester { SemesterId = "Fall"});
            if (!context.Semesters.Any(x => x.SemesterId == "Spring"))
                context.Semesters.Add(new Semester { SemesterId = "Spring"});
            context.SaveChanges();
        }

        public static void SeedCourse(ApplicationDbContext context)
        {
            if (!context.Courses.Any(x => x.CourseId == "CIDM4390"))
                context.Courses.Add(new Course { CourseId = "CIDM4390", CourseName = "Software Systems Development"});
            if (!context.Courses.Any(x => x.CourseId == "CIDM3320"))
                context.Courses.Add(new Course { CourseId = "CIDM3320", CourseName = "Digital Communication & Collaboration"});
            context.SaveChanges();
        }

        public static void SeedCourseInstance(ApplicationDbContext context)
        {
            if (!context.CourseInstances.Any(x => x.CourseInstanceId == "CIDM4390-01"))
                context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "CIDM4390-01", AcademicYearId = "2020-2021", SemesterId = "Spring", CourseId = "CIDM4390"});
            if (!context.CourseInstances.Any(x => x.CourseInstanceId == "CIDM3320-70"))
                context.CourseInstances.Add(new CourseInstance { CourseInstanceId = "CIDM3320-70", AcademicYearId = "2020-2021", SemesterId = "Spring", CourseId = "CIDM3320"});
            context.SaveChanges();
        }

        public static void SeedCourseLearningOutcome(ApplicationDbContext context)
        {
            if (!context.CourseLearningOutcomes.Any(x => x.CourseLearningOutcomeId == "CLO1"))
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO1", CourseLearningOutcomeDesc = "Apply a visualization design process to create effective visualizations.",CourseInstanceId = "CIDM4390-01", CacStudentOutcomeId = "SO1", AssessmentId = 6458});
            if (!context.CourseLearningOutcomes.Any(x => x.CourseLearningOutcomeId == "CLO2"))
                context.CourseLearningOutcomes.Add(new CourseLearningOutcome { CourseLearningOutcomeId = "CLO2", CourseLearningOutcomeDesc = "Apply a visualization design process to create effective visualizations.",CourseInstanceId = "CIDM3320-70", CacStudentOutcomeId = "SO2", AssessmentId = 4891});
            context.SaveChanges();
        }

        public static void SeedAssessment(ApplicationDbContext context)
        {
            if (!context.Assessments.Any(x => x.AssessmentId == 6458))
                context.Assessments.Add(new Assessment { AssessmentId = 6458, AssessmentInstrument = "Quiz 1", ExcellentStudents = 12, GoodStudents = 7, SatisfactoryStudents = 3, UnsatisfactoryStudents = 6, MissingStudents = 2, TotalStudents = 30, CourseLearningOutcomeId = "CLO1"});
            if (!context.Assessments.Any(x => x.AssessmentId == 4891))
                context.Assessments.Add(new Assessment { AssessmentId = 4891, AssessmentInstrument = "Homework 3", ExcellentStudents = 7, GoodStudents = 6, SatisfactoryStudents = 12, UnsatisfactoryStudents = 2, MissingStudents = 3, TotalStudents = 30, CourseLearningOutcomeId = "CLO2"});
            context.SaveChanges();
        }

        public static void SeedFacultyQualitativeAssessmentReport(ApplicationDbContext context)
        {
            if (!context.FacultyQualitativeAssessmentReports.Any(x => x.FacultyQualitativeAssessmentReportId == 1))
                context.FacultyQualitativeAssessmentReports.Add(new FacultyQualitativeAssessmentReport { FacultyQualitativeAssessmentReportId = 1, New = "Added review assignments for past courses in Week 1 and 2.", Observations = "Students successfully showed recollection of previous material.", StudentFeedback = "Student liked the review assignments and the integration the material of those assignments had during the course.", Change = "To add quizzes throughout the semester to test knowledge retention.", CourseInstanceId = "CIDM4390-01"});
            if (!context.FacultyQualitativeAssessmentReports.Any(x => x.FacultyQualitativeAssessmentReportId == 2))
                context.FacultyQualitativeAssessmentReports.Add(new FacultyQualitativeAssessmentReport { FacultyQualitativeAssessmentReportId = 2, New = "Added project 3 which is a simulation that students do together to demonstrate virtual teamwork.", Observations = "Students were able to successfully demonstrate virtual teamwork with similar projects.", StudentFeedback = "Students liked the project as they were able to apply critical thinking while also maximizing their team performance.", Change = "A midterm exam will be added to test knowledge retention.", CourseInstanceId = "CIDM3320-70"});
            context.SaveChanges();
        }
    }
}