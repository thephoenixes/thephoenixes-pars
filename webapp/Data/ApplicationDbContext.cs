﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using domain;
using domain.AcademicYearAggregate;
using domain.AssessmentAggregate;
using domain.CacstudentOutcomeAggregate;
using domain.CourseAggregate;
using domain.CourseInstanceAggregate;
using domain.CourseInstanceCacstudentOutcomeAggregate;
using domain.CourseLearningOutcomeAggregate;
using domain.ProgramEducationalObjectiveAggregate;
using domain.SemesterAggregate;
using domain.FacultyQualitativeAssessmentReportAggregate;

namespace webapp.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        private readonly Action<ApplicationDbContext, ModelBuilder> _customizeModel;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, Action<ApplicationDbContext, ModelBuilder> customizeModel)
        : base(options)
        {
            // customizeModel must be the same for every instance in a given application.
            // Otherwise a custom IModelCacheKeyFactory implementation must be provided.
            _customizeModel = customizeModel;
        }

        public virtual DbSet<AcademicYear> AcademicYears { get; set; }
        public virtual DbSet<Assessment> Assessments { get; set; }
        public virtual DbSet<CacstudentOutcome> CacstudentOutcomes { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<CourseInstance> CourseInstances { get; set; }
        public virtual DbSet<CourseInstanceCacstudentOutcome> CourseInstanceCacstudentOutcomes { get; set; }
        public virtual DbSet<CourseLearningOutcome> CourseLearningOutcomes { get; set; }
        public virtual DbSet<ProgramEducationalObjective> ProgramEducationalObjectives { get; set; }
        public virtual DbSet<Semester> Semesters { get; set; }
        public virtual DbSet<FacultyQualitativeAssessmentReport> FacultyQualitativeAssessmentReports { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<AcademicYear>(entity =>
            {
                entity.ToTable("AcademicYear");

                entity.Property(e => e.AcademicYearId).HasColumnName("academicYearID");
            });

            base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>(entity =>
            {
                entity.ToTable(name: "User");
            });


            builder.Entity<Assessment>(entity =>
            {
                entity.ToTable("Assessment");

                entity.HasIndex(e => e.AssessmentId, "IX_Assessment_assessmentID")
                    .IsUnique();

                entity.Property(e => e.AssessmentId).HasColumnName("assessmentID");

                entity.Property(e => e.CourseLearningOutcomeId).HasColumnName("courseLearningOutcomeID");

                entity.Property(e => e.AssessmentInstrument).HasColumnName("assessmentInstrument");

                entity.Property(e => e.ExcellentStudents).HasColumnName("excellentStudents");

                entity.Property(e => e.GoodStudents).HasColumnName("goodStudents");

                entity.Property(e => e.SatisfactoryStudents).HasColumnName("satisfactoryStudents");

                entity.Property(e => e.UnsatisfactoryStudents).HasColumnName("unsatisfactoryStudents");

                entity.Property(e => e.MissingStudents).HasColumnName("missingStudents");

                entity.Property(e => e.TotalStudents).HasColumnName("totalStudents");

                entity.HasOne(d => d.CourseLearningOutcome)
                    .WithMany(p => p.Assessments)
                    .HasForeignKey(d => d.CourseLearningOutcomeId);
            });

            builder.Entity<CacstudentOutcome>(entity =>
            {
                entity.ToTable("CACStudentOutcome");

                entity.Property(e => e.CacStudentOutcomeId).HasColumnName("cacStudentOutcomeID");

                entity.Property(e => e.CacStudentOutcomeDesc).HasColumnName("cacStudentOutcomeDesc");
            });

            builder.Entity<Course>(entity =>
            {
                entity.ToTable("Course");

                entity.Property(e => e.CourseId).HasColumnName("CourseID");

                entity.Property(e => e.CourseName).HasColumnName("courseName");
            });

            builder.Entity<CourseInstance>(entity =>
            {
                entity.ToTable("CourseInstance");

                entity.HasIndex(e => e.CourseInstanceId, "IX_CourseInstance_courseInstanceID")
                    .IsUnique();

                entity.Property(e => e.CourseInstanceId).HasColumnName("courseInstanceID");

                entity.Property(e => e.AcademicYearId).HasColumnName("academicYearID");

                entity.Property(e => e.CourseId).HasColumnName("courseID");

                entity.Property(e => e.SemesterId).HasColumnName("semesterID");

                entity.Property(e => e.FacultyQualitativeAssessmentReportId).HasColumnName("facultyQualitativeAssessmentReportID");

                entity.HasOne(d => d.AcademicYear)
                    .WithMany(p => p.CourseInstances)
                    .HasForeignKey(d => d.AcademicYearId);

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.CourseInstances)
                    .HasForeignKey(d => d.CourseId);

                entity.HasOne(d => d.Semester)
                    .WithMany(p => p.CourseInstances)
                    .HasForeignKey(d => d.SemesterId);

                entity.HasOne<FacultyQualitativeAssessmentReport>(d => d.FacultyQualitativeAssessmentReport)
                    .WithOne(p => p.CourseInstance)
                    .HasForeignKey<CourseInstance>(d => d.FacultyQualitativeAssessmentReportId);
            });

            builder.Entity<CourseInstanceCacstudentOutcome>(entity =>
            {
                entity.HasKey(e => new { e.CourseInstanceId, e.CacStudentOutcomeId });

                entity.ToTable("CourseInstance-CACStudentOutcome");

                entity.HasIndex(e => e.CacStudentOutcomeId, "IX_CourseInstance-CACStudentOutcome_cacStudentOutcomeID");

                entity.HasIndex(e => e.CourseInstanceId, "IX_CourseInstance-CACStudentOutcome_courseInstanceID");

                entity.Property(e => e.CourseInstanceId).HasColumnName("courseInstanceID");

                entity.Property(e => e.CacStudentOutcomeId).HasColumnName("cacStudentOutcomeID");

                entity.HasOne(d => d.CacStudentOutcome)
                    .WithMany(p => p.CourseInstanceCacstudentOutcomes)
                    .HasForeignKey(d => d.CacStudentOutcomeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.CourseInstance)
                    .WithMany(p => p.CourseInstanceCacstudentOutcomes)
                    .HasForeignKey(d => d.CourseInstanceId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            builder.Entity<CourseLearningOutcome>(entity =>
            {
                entity.ToTable("CourseLearningOutcome");

                entity.HasIndex(e => e.CourseLearningOutcomeId, "IX_CourseLearningOutcome_courseLearningOutcomeID")
                    .IsUnique();

                entity.Property(e => e.CourseLearningOutcomeId).HasColumnName("courseLearningOutcomeID");

                entity.Property(e => e.AssessmentId).HasColumnName("assessmentID");

                entity.Property(e => e.CacStudentOutcomeId).HasColumnName("cacStudentOutcomeID");

                entity.Property(e => e.CourseInstanceId).HasColumnName("courseInstanceID");

                entity.Property(e => e.CourseLearningOutcomeDesc).HasColumnName("courseLearningOutcomeDesc");

                entity.HasOne(d => d.CacStudentOutcome)
                    .WithMany(p => p.CourseLearningOutcomes)
                    .HasForeignKey(d => d.CacStudentOutcomeId);

                entity.HasOne(d => d.CourseInstance)
                    .WithMany(p => p.CourseLearningOutcomes)
                    .HasForeignKey(d => d.CourseInstanceId);
            });

            builder.Entity<IdentityRole>(entity =>
            {
                entity.ToTable(name: "Role");
            });
            builder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.ToTable("UserRoles");
            });

            builder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.ToTable("UserClaims");
            });

            builder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.ToTable("UserLogins");
            });

            builder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.ToTable("RoleClaims");

            });

            builder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.ToTable("UserTokens");
            });

            builder.Entity<ProgramEducationalObjective>(entity =>
            {
                entity.ToTable("ProgramEducationalObjective");

                entity.Property(e => e.ProgramEducationalObjectiveId).HasColumnName("programEducationalObjectiveID");

                entity.Property(e => e.ProgramEducationalObjectiveDesc).HasColumnName("programEducationalObjectiveDesc");
            });

            builder.Entity<Semester>(entity =>
            {
                entity.ToTable("Semester");

                entity.Property(e => e.SemesterId).HasColumnName("semesterID");
            });

            builder.Entity<FacultyQualitativeAssessmentReport>(entity =>
            {
                entity.ToTable("FacultyQualitativeAssessmentReport");

                entity.HasIndex(e => e.FacultyQualitativeAssessmentReportId, "IX_FacultyQualitativeAssessmentReport_FacultyQualitativeAssessmentReportID")
                    .IsUnique();

                entity.Property(e => e.FacultyQualitativeAssessmentReportId).HasColumnName("facultyQualitativeAssessmentReportID");

                entity.Property(e => e.New).HasColumnName("new");

                entity.Property(e => e.Observations).HasColumnName("observations");

                entity.Property(e => e.StudentFeedback).HasColumnName("studentFeedback");

                entity.Property(e => e.Change).HasColumnName("change");

                entity.Property(e => e.CourseInstanceId).HasColumnName("courseInstanceID");

                entity.HasOne<CourseInstance>(d => d.CourseInstance)
                    .WithOne(p => p.FacultyQualitativeAssessmentReport)
                    .HasForeignKey<FacultyQualitativeAssessmentReport>(d => d.CourseInstanceId);
            });
        }
    }
}
