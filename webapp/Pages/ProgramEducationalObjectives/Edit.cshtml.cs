using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using domain.ProgramEducationalObjectiveAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_ProgramEducationalObjectives
{
    [Authorize (Roles = "Admin")]
    public class EditModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public EditModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ProgramEducationalObjective ProgramEducationalObjective { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProgramEducationalObjective = await _context.ProgramEducationalObjectives.FirstOrDefaultAsync(m => m.ProgramEducationalObjectiveId == id);

            if (ProgramEducationalObjective == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(ProgramEducationalObjective).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgramEducationalObjectiveExists(ProgramEducationalObjective.ProgramEducationalObjectiveId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ProgramEducationalObjectiveExists(string id)
        {
            return _context.ProgramEducationalObjectives.Any(e => e.ProgramEducationalObjectiveId == id);
        }
    }
}
