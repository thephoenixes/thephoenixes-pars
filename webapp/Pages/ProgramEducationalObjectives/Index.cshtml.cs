using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.ProgramEducationalObjectiveAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_ProgramEducationalObjectives
{
    [Authorize (Roles = "Admin, Evaluator, Professor")]
    public class IndexModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public IndexModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<ProgramEducationalObjective> ProgramEducationalObjective { get;set; }

        public async Task OnGetAsync()
        {
            ProgramEducationalObjective = await _context.ProgramEducationalObjectives.ToListAsync();
        }
    }
}
