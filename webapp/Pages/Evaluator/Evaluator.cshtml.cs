using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages
{
    [Authorize (Roles = "Evaluator, Admin")]
    public class EvaluatorModel : PageModel
    {
         private readonly ILogger<EvaluatorModel> _logger;

        public EvaluatorModel(ILogger<EvaluatorModel> logger)
        {
            _logger = logger;
        }
 
        public void OnGet()
        {
        }
    }
}