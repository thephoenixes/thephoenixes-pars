using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.AcademicYearAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_AcademicYears
{
    [Authorize (Roles = "Admin")]
    public class DetailsModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public DetailsModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public AcademicYear AcademicYear { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AcademicYear = await _context.AcademicYears.FirstOrDefaultAsync(m => m.AcademicYearId == id);

            if (AcademicYear == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
