using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.CacstudentOutcomeAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;


namespace webapp.Pages_StudentOutcome
{
    [Authorize (Roles = "Admin")]

    public class DetailsModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public DetailsModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public CacstudentOutcome CacstudentOutcome { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CacstudentOutcome = await _context.CacstudentOutcomes.FirstOrDefaultAsync(m => m.CacStudentOutcomeId == id);

            if (CacstudentOutcome == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
