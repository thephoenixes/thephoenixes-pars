using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.FacultyQualitativeAssessmentReportAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_FacultyQualitativeAssessmentReport
{
    [Authorize (Roles = "Admin, Professor, Evaluator")]
    public class IndexModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public IndexModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<FacultyQualitativeAssessmentReport> FacultyQualitativeAssessmentReport { get;set; }

        public async Task OnGetAsync()
        {
            FacultyQualitativeAssessmentReport = await _context.FacultyQualitativeAssessmentReports
                .Include(f => f.CourseInstance).ToListAsync();
        }
    }
}
