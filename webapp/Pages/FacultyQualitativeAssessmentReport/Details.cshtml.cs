using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.FacultyQualitativeAssessmentReportAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_FacultyQualitativeAssessmentReport
{
    [Authorize (Roles = "Admin")]
    public class DetailsModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public DetailsModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public FacultyQualitativeAssessmentReport FacultyQualitativeAssessmentReport { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FacultyQualitativeAssessmentReport = await _context.FacultyQualitativeAssessmentReports
                .Include(f => f.CourseInstance).FirstOrDefaultAsync(m => m.FacultyQualitativeAssessmentReportId == id);

            if (FacultyQualitativeAssessmentReport == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
