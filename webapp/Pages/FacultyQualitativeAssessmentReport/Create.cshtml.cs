using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using domain.FacultyQualitativeAssessmentReportAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_FacultyQualitativeAssessmentReport
{
    [Authorize (Roles = "Admin, Professor")]
    public class CreateModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public CreateModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["CourseInstanceId"] = new SelectList(_context.CourseInstances, "CourseInstanceId", "CourseInstanceId");
            return Page();
        }

        [BindProperty]
        public FacultyQualitativeAssessmentReport FacultyQualitativeAssessmentReport { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.FacultyQualitativeAssessmentReports.Add(FacultyQualitativeAssessmentReport);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
