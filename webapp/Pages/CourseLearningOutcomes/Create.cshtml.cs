using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using domain.CourseLearningOutcomeAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;


namespace webapp.Pages_CourseLearningOutcomes
{
    [Authorize (Roles = "Admin, Professor")]
    public class CreateModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public CreateModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["CacStudentOutcomeId"] = new SelectList(_context.CacstudentOutcomes, "CacStudentOutcomeId", "CacStudentOutcomeId");
        ViewData["CourseInstanceId"] = new SelectList(_context.CourseInstances, "CourseInstanceId", "CourseInstanceId");
            return Page();
        }

        [BindProperty]
        public CourseLearningOutcome CourseLearningOutcome { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.CourseLearningOutcomes.Add(CourseLearningOutcome);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
