using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using domain.CourseLearningOutcomeAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;


namespace webapp.Pages_CourseLearningOutcomes
{
    [Authorize (Roles = "Admin")]
    public class EditModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public EditModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public CourseLearningOutcome CourseLearningOutcome { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CourseLearningOutcome = await _context.CourseLearningOutcomes
                .Include(c => c.CacStudentOutcome)
                .Include(c => c.CourseInstance).FirstOrDefaultAsync(m => m.CourseLearningOutcomeId == id);

            if (CourseLearningOutcome == null)
            {
                return NotFound();
            }
           ViewData["CacStudentOutcomeId"] = new SelectList(_context.CacstudentOutcomes, "CacStudentOutcomeId", "CacStudentOutcomeId");
           ViewData["CourseInstanceId"] = new SelectList(_context.CourseInstances, "CourseInstanceId", "CourseInstanceId");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(CourseLearningOutcome).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CourseLearningOutcomeExists(CourseLearningOutcome.CourseLearningOutcomeId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CourseLearningOutcomeExists(string id)
        {
            return _context.CourseLearningOutcomes.Any(e => e.CourseLearningOutcomeId == id);
        }
    }
}
