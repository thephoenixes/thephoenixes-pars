using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.CourseLearningOutcomeAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;


namespace webapp.Pages_CourseLearningOutcomes
{
    [Authorize (Roles = "Admin")]
    public class DetailsModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public DetailsModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public CourseLearningOutcome CourseLearningOutcome { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CourseLearningOutcome = await _context.CourseLearningOutcomes
                .Include(c => c.CacStudentOutcome)
                .Include(c => c.CourseInstance).FirstOrDefaultAsync(m => m.CourseLearningOutcomeId == id);

            if (CourseLearningOutcome == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
