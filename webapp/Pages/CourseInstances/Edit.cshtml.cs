using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using domain.CourseInstanceAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_
{
    [Authorize (Roles = "Admin")]
    public class EditModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public EditModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public CourseInstance CourseInstance { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CourseInstance = await _context.CourseInstances
                .Include(c => c.AcademicYear)
                .Include(c => c.Course)
                .Include(c => c.Semester).FirstOrDefaultAsync(m => m.CourseInstanceId == id);

            if (CourseInstance == null)
            {
                return NotFound();
            }
           ViewData["AcademicYearId"] = new SelectList(_context.AcademicYears, "AcademicYearId", "AcademicYearId");
           ViewData["CourseId"] = new SelectList(_context.Courses, "CourseId", "CourseId");
           ViewData["SemesterId"] = new SelectList(_context.Semesters, "SemesterId", "SemesterId");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(CourseInstance).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CourseInstanceExists(CourseInstance.CourseInstanceId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CourseInstanceExists(string id)
        {
            return _context.CourseInstances.Any(e => e.CourseInstanceId == id);
        }
    }
}
