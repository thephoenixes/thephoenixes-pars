using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.CourseInstanceAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_
{
    [Authorize (Roles = "Admin")]
    public class DetailsModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public DetailsModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public CourseInstance CourseInstance { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CourseInstance = await _context.CourseInstances
                .Include(c => c.AcademicYear)
                .Include(c => c.Course)
                .Include(c => c.Semester).FirstOrDefaultAsync(m => m.CourseInstanceId == id);

            if (CourseInstance == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
