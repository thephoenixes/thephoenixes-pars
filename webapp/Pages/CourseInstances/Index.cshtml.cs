using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.CourseInstanceAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_
{
    [Authorize (Roles = "Admin, Evaluator, Professor")]
    public class IndexModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public IndexModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<CourseInstance> CourseInstance { get;set; }

        public async Task OnGetAsync()
        {
            CourseInstance = await _context.CourseInstances
                .Include(c => c.AcademicYear)
                .Include(c => c.Course)
                .Include(c => c.Semester).ToListAsync();
        }
    }
}
