using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using domain.CourseInstanceAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_
{
    [Authorize (Roles = "Admin, Professor")]
    public class CreateModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public CreateModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["AcademicYearId"] = new SelectList(_context.AcademicYears, "AcademicYearId", "AcademicYearId");
        ViewData["CourseId"] = new SelectList(_context.Courses, "CourseId", "CourseId");
        ViewData["SemesterId"] = new SelectList(_context.Semesters, "SemesterId", "SemesterId");
            return Page();
        }

        [BindProperty]
        public CourseInstance CourseInstance { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.CourseInstances.Add(CourseInstance);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
