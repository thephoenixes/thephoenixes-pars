using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using domain.CourseAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;


namespace webapp.Pages_Courses
{
    [Authorize (Roles = "Admin")]
    public class DetailsModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public DetailsModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Course Course { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Course = await _context.Courses.FirstOrDefaultAsync(m => m.CourseId == id);

            if (Course == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
