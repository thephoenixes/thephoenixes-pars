using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using domain.AssessmentAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_Assessments
{
    [Authorize (Roles = "Admin")]
    public class EditModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public EditModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Assessment Assessment { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Assessment = await _context.Assessments
                .Include(a => a.CourseLearningOutcome).FirstOrDefaultAsync(m => m.AssessmentId == id);

            if (Assessment == null)
            {
                return NotFound();
            }
           
            ViewData["CourseLearningOutcomeId"] = new SelectList(_context.CourseLearningOutcomes, "CourseLearningOutcomeId", "CourseLearningOutcomeId");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                ViewData["CourseLearningOutcomeId"] = new SelectList(_context.CourseLearningOutcomes, "CourseLearningOutcomeId", "CourseLearningOutcomeId");
                return Page();
            }

            _context.Attach(Assessment).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssessmentExists(Assessment.AssessmentId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool AssessmentExists(long id)
        {
            return _context.Assessments.Any(e => e.AssessmentId == id);
        }
    }
}
