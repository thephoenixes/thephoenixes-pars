using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using domain.AssessmentAggregate;
using webapp.Data;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages_Assessments
{
    [Authorize (Roles = "Admin, Professor")]
    public class CreateModel : PageModel
    {
        private readonly webapp.Data.ApplicationDbContext _context;

        public CreateModel(webapp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["CourseLearningOutcomeId"] = new SelectList(_context.CourseLearningOutcomes, "CourseLearningOutcomeId", "CourseLearningOutcomeId");
            return Page();
        }

        [BindProperty]
        public Assessment Assessment { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Assessments.Add(Assessment);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
