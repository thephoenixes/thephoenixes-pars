using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace webapp.Pages
{
    [Authorize (Roles = "Professor, Admin")]
    public class ProfessorModel : PageModel
    {
         private readonly ILogger<ProfessorModel> _logger;

        public ProfessorModel(ILogger<ProfessorModel> logger)
        {
            _logger = logger;
        }
 
        public void OnGet()
        {
        }
    }
}