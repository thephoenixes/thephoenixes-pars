using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using webapp.Data;
using domain;
using webapp.Enums;
using Microsoft.EntityFrameworkCore;

namespace webapp
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var loggerFactory = services.GetRequiredService<ILoggerFactory>();
                try
                {
                    var context = services.GetRequiredService<ApplicationDbContext>();
                    context.Database.Migrate();

                    var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
                    var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

                    // Seed Identity
                    await ContextSeed.SeedRolesAsync(userManager, roleManager);
                    await ContextSeed.SeedSuperAdminAsync(userManager, roleManager);
                    await ContextSeed.SeedProfessorAsync(userManager, roleManager);
                    await ContextSeed.SeedEvaluatorAsync(userManager, roleManager);

                    //  Seed PARS database
                    ContextSeed.SeedAcademicYear(context);
                    ContextSeed.SeedProgramEducationalObjective(context);
                    ContextSeed.SeedStudentOutcome(context);
                    ContextSeed.SeedCourse(context);
                    ContextSeed.SeedSemester(context);
                    ContextSeed.SeedCourseInstance(context);
                    ContextSeed.SeedCourseLearningOutcome(context);
                    ContextSeed.SeedAssessment(context);
                    ContextSeed.SeedFacultyQualitativeAssessmentReport(context);
                }
                catch (Exception ex)
                {
                    var logger = loggerFactory.CreateLogger<Program>();
                    logger.LogError(ex, "An error occurred seeding the DB.");
                }
            }
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}