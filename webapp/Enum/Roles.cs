using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapp.Enums
{
    public enum Roles
    {
        Admin,
        Professor,
        Evaluator
    }
}